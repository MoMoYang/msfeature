﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FileReader;
using PeptideFeature;
//using pwiz.CLI.msdata;

namespace MSFeature
{
    public partial class Form1 : Form
    {
        private readonly double _charge_max = 5.6d;
        private readonly double _charge_min = 5.5d;
        //private MSDataFile _currentRawdata = null;
        private readonly string _outputdir = @"D:\Lab\Result\MSFeature\numbers";
        private readonly ProjectManager _projectManager;
        private readonly UC_Projectlist _projectUC = new UC_Projectlist();
        //private MSPlot _msPlot = null;
        //private Project _currentProject = null;


        //private FeatureManager _currentFeatureManager = null;
        //private Kronik _currentKronik = null;
        //private Percolator _currentPercolator = null;
        //private MS2TitleInfo _currentIsolatedRegions = null;
        //private MS2TitleInfo _currentNoMatchScans = null;
        //private RawDataInfo _currentRaw = null;
        //private Hardklor _currentHardklor = null;
        private Dictionary<string, FileManager> _files = new Dictionary<string, FileManager>();
        private double _maxMZ = 1330d;
        private double _maxRT = 60d;
        private double _minMZ = 1290d;
        private double _minRT = 10d;

        public Form1()
        {
            InitializeComponent();


            _projectManager = new ProjectManager();
            _projectManager.ProjectListChanged += _projectUC.UpdateProjectList;
            _projectUC.SelectedProjectChanged += _projectManager.ChangeCurrentProject;
        }

        private FileManager GetFileManager(string fileprefix)
        {
            if (!_files.ContainsKey(fileprefix))
                _files.Add(fileprefix, new FileManager(fileprefix));
            return _files[fileprefix];
        }

        private void openKronicResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var op = new OpenFileDialog();
            op.Filter = "*.kk (Kronik result)|*.kk";
            op.Multiselect = true;

            if (op.ShowDialog() == DialogResult.OK)
            {
                foreach (var filepath in op.FileNames)
                {
                    var fileprefix = Path.GetFileNameWithoutExtension(filepath);
                    GetFileManager(fileprefix).AddKronikFile(filepath);
                    Console.WriteLine("Kronik file added: " + fileprefix);
                }
                //_currentKronik = new Kronik(op.FileName, Path.GetFileNameWithoutExtension(op.FileName));
            }
        }

        private void openPercolatorResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var op = new OpenFileDialog();
            op.Filter = "*.perc.xml (Percolator result)|*.xml";
            op.Multiselect = true;

            if (op.ShowDialog() == DialogResult.OK)
            {
                foreach (var filepath in op.FileNames)
                {
                    var fileprefix = Path.GetFileNameWithoutExtension(filepath);
                    fileprefix = fileprefix.Substring(0, fileprefix.IndexOf('.'));
                    GetFileManager(fileprefix).AddSearchresult(filepath);
                    Console.WriteLine("Search file added: " + fileprefix);
                }
                //_currentPercolator = new Percolator(op.FileName, Path.GetFileNameWithoutExtension(op.FileName));
            }
        }

        private void openMS2FileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var op = new OpenFileDialog();
            op.FileName = "*.ms2";
            op.Multiselect = true;
            if (op.ShowDialog() == DialogResult.OK)
            {
                foreach (var filepath in op.FileNames)
                {
                    var fileprefix = Path.GetFileNameWithoutExtension(filepath);
                    fileprefix = fileprefix.Substring(0, fileprefix.IndexOf('.'));
                    GetFileManager(fileprefix).AddMS2file(filepath);
                    Console.WriteLine("Ms2 file added: " + fileprefix);
                }
                //_currentIsolatedRegions = new MS2TitleInfo(op.FileName, Path.GetFileNameWithoutExtension(op.FileName));
            }
        }

        private void openNoMatchedMS2FileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var op = new OpenFileDialog();
            op.FileName = "*.ms2";
            op.Multiselect = true;
            if (op.ShowDialog() == DialogResult.OK)
            {
                foreach (var filepath in op.FileNames)
                {
                    var fileprefix = Path.GetFileNameWithoutExtension(filepath);
                    fileprefix = fileprefix.Substring(0, fileprefix.IndexOf('.'));
                    GetFileManager(fileprefix).AddNonMatchedMS2(filepath);
                    Console.WriteLine("NoMatched file added: " + fileprefix);
                }
                //_currentNoMatchScans = new MS2TitleInfo(op.FileName, Path.GetFileNameWithoutExtension(op.FileName));
            }
        }

        private void openRawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var op = new OpenFileDialog();
            op.Title = "Please select raw file";
            op.Filter = "*.raw|*.raw";
            op.Multiselect = true;

            if (op.ShowDialog() == DialogResult.OK)
            {
                foreach (var filepath in op.FileNames)
                {
                    var fileprefix = Path.GetFileNameWithoutExtension(filepath);
                    GetFileManager(fileprefix).AddRawfile(filepath);
                    Console.WriteLine("Raw file added: " + fileprefix);
                }
                //_currentRaw = FileReader.RawReader.ReadRawfileHeader(op.FileName);
                // i need some information here
            }

            #region Output raw file infomation

            //string outfile = AppDomain.CurrentDomain.BaseDirectory + @"/rawfiles_record.txt";
            //if (op.ShowDialog() == DialogResult.OK)
            //{
            //    StreamWriter sw = new StreamWriter(outfile);
            //    sw.WriteLine("raw file\tTotal scan\tTotal MS1\tTotal MS2\t# instrument scan\t#custom MS1\t#custom MS2\t#periodic MS1\tIon Injection time of MS1\tIon Injection time of MS2");
            //    // mask for now
            //    //OpenRawUsingProteomeWizard();
            //    StringBuilder sb = new StringBuilder();
            //    foreach (string filepath in op.FileNames)
            //    {
            //        RawDataInfo rawdata = OpenRawUsingTheromDLL(filepath);
            //        string filename = Path.GetFileNameWithoutExtension(filepath);
            //        sb.Append(filename + "\t");
            //        sb.Append(rawdata.Statistics.NumTotalScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.NumMSoneScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.NumMStwoScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.NumInstrumentMSoneScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.NumCustomMSoneScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.NumCustomMStwoScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.NumPeriodicMSoneScan.ToString() + "\t");
            //        sb.Append(rawdata.Statistics.AverageIonjectionTimeMS1.ToString() + "\t");
            //        sb.AppendLine(rawdata.Statistics.AverageIonjectionTimeMS2.ToString());
            //    }
            //    sw.Write(sb.ToString());
            //    sw.Close();
            //}

            #endregion
        }

        // remove this later


        private void openHardklorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //_currentHardklor = null;
            var op = new OpenFileDialog();
            op.Filter = "*.hk|*.hk";
            op.Multiselect = true;

            if (op.ShowDialog() == DialogResult.OK)
            {
                foreach (var filepath in op.FileNames)
                {
                    var fileprefix = Path.GetFileNameWithoutExtension(filepath);
                    GetFileManager(fileprefix).AddHardklorFile(filepath);
                    Console.WriteLine("Hardklor file added: " + fileprefix);
                }
                //_currentHardklor = new Hardklor(op.FileName, Path.GetFileNameWithoutExtension(op.FileName));
            }
        }

        private void countToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //
            // modify this later
            //
            foreach (var filepreix in _files.Keys)
            {
                var fmanager = _files[filepreix];
                if (fmanager.Kronik == null)
                    continue;
                var featureManager = new FeatureManager(fmanager.Percolator, fmanager.MS2, fmanager.Kronik,
                    fmanager.Hardklor);
                var newProject = new Project(featureManager);
                newProject.UpdateRawDataInfo(fmanager.Raw);
                newProject.UpdateFailAssignedRegion(fmanager.NoMatched);
                _projectManager.AddProject(newProject.ID, newProject);
                //_currentHardklor = null;
                //_currentIsolatedRegions = null;
                //_currentKronik = null;
                //_currentNoMatchScans = null;
                //_currentPercolator = null;
                //_currentRaw = null;
                Console.WriteLine("Added project: " + filepreix);
            }
            _files = new Dictionary<string, FileManager>();
            //if (_currentRawdata!=null)
            //    _currentFeatureManager.SetRawFile(_currentRawdata);
        }

        private void toolStripButtonStatistics_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            if (_projectManager.CurrentProject.FeatureManager != null)
            {
                sb.AppendLine(_projectManager.CurrentProject.FeatureManager.Message());
            }
            else
            {
                sb.Append("No informaiton is input");
            }
            MessageBox.Show(sb.ToString());
        }

        private void OpenRawUsingProteomeWizard(string filepath)
        {
            //_currentRawdata = new MSDataFile(op.FileName);
            //if (_currentFeatureManager!=null)
            //    _currentFeatureManager.SetRawFile(_currentRawdata);
            //int size = _currentRawdata.run.spectrumList.size();
            //MessageBox.Show(size.ToString());
        }

        private void pIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pif = new PIFCalculator();
            
            //, _projectManager.CurrentProject.RawInfo 
            // mask for now
            string rawfile = _projectManager.CurrentProject.RawInfo.Filepath;
            if (!File.Exists(rawfile))
                MessageBox.Show("no selected rawfile");
            else
                pif.SetupPrecursorScans(_projectManager.CurrentProject.FeatureManager, _projectManager.CurrentProject.RawInfo.Filepath);
        }

        private void toolStripButton_project_Click(object sender, EventArgs e)
        {
            var projectF = new Form();
            projectF.Text = "Project list";
            projectF.Size = new Size(_projectUC.Size.Width + 15, _projectUC.Size.Height + 30);
            _projectUC.Dock = DockStyle.Fill;
            projectF.Controls.Add(_projectUC);
            projectF.Closing += projectF_Closing;
            projectF.Show();
        }

        private void projectF_Closing(object sender, CancelEventArgs e)
        {
            ((Form) sender).Controls.Remove(_projectUC);
        }

        private void outputProjectStatisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Output.ProjectStatistics(_projectManager.AllProjects.ToList(), _outputdir + @"\ProjectStatistics");
            MessageBox.Show("Output file:\n" + _outputdir + @"\ProjectStatistics");
        }

        private void mS1OverTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject.RawInfo == null)
                return;

            var rtValue = new List<double>();
            foreach (var scan in _projectManager.CurrentProject.RawInfo.Scanlist)
            {
                if (scan.ScanOrder != 1)
                    continue;
                rtValue.Add(scan.RetentionTime);
            }
            _msPlot.PlotHistogram(rtValue, "MS1 scan", "RT", 1d);
        }

        private void featureRTCrossMZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rt = Parameter.ElutingRTMin;
            _msPlot.PlotFeatureRTAcrossMZ(
                _projectManager.CurrentProject.FeatureManager.PeptideFeatures(Parameter.RT_min_cutoff,
                    Parameter.RT_max_cutoff, rt).ToList());
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject != null)
                GroupScansByFeatures.AssignGroup(_projectManager.CurrentProject.FeatureManager);
        }

        private void plotAllFiguresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var project in _projectManager.AllProjects)
            {
                _projectManager.CurrentProject = project;
                var filename = _outputdir + _projectManager.CurrentProject.ID;
                _msPlot.ClearCurves();
                featureMzPlotToolStripMenuItem_Click(new object(), new EventArgs());

                _msPlot.SaveCurrentPic(filename + "_mzPlot.png");
                _msPlot.ClearCurves();
                sampledMzRegionPlotToolStripMenuItem_Click(new object(), new EventArgs());
                _msPlot.SaveCurrentPic(filename + "_mzRegion.png");

                _msPlot.ClearCurves();
                featureNumberAcrossRTToolStripMenuItem_Click(new object(), new EventArgs());
                _msPlot.SaveCurrentPic(filename + "_FeatureRT.png");

                _msPlot.ClearCurves();
                featureRTDistributionToolStripMenuItem_Click(new object(), new EventArgs());
                _msPlot.SaveCurrentPic(filename + "_FeatureElution.png");

                _msPlot.ClearCurves();
                featureRTCrossMZToolStripMenuItem_Click(new object(), new EventArgs());
                _msPlot.SaveCurrentPic(filename + "_FeatureElutionMZ.png");

                _msPlot.ClearCurves();
                mS1OverTimeToolStripMenuItem_Click(new object(), new EventArgs());
                _msPlot.SaveCurrentPic(filename + "_MS1cT.png");

                _msPlot.ClearCurves();
                mS2AcrossRTToolStripMenuItem_Click(new object(), new EventArgs());
                _msPlot.SaveCurrentPic(filename + "_MS2cT.png");
            }
        }

        private void analysisTimesDistributionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var count = new List<double>();
            //List<double> id = new List<double>();
            //List<double> noid = new List<double>();

            var rt = Parameter.ElutingRTMin;
            foreach (
                var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(0, Parameter.RT_max_cutoff, rt)
                )
            {
                if (pep.Charge == 1)
                    continue;
                if (!_projectManager.CurrentProject.FeatureManager.IsAnalyzed(pep))
                    continue;
                //if (_projectManager.CurrentProject.FeatureManager.IsIdentified(pep))
                //    id.Add(_projectManager.CurrentProject.FeatureManager.FeatureToScans(pep).Count);
                //else
                //    noid.Add(_projectManager.CurrentProject.FeatureManager.FeatureToScans(pep).Count);
                count.Add(_projectManager.CurrentProject.FeatureManager.FeatureToScans(pep).Count);
            }
            var ylabel = "Count of Features";
            var axislabel = "Count of MS2 scans";

            var title = _projectManager.CurrentProject.ID;
            _msPlot.PlotHistogram(count, _projectManager.CurrentProject.ID, "# MS2", 0.1);
            //double percentage = Math.Round((double)id.Count/(id.Count + noid.Count),4)*100;
            //_msPlot.PlotHistogram(id,"Id ions",axislabel,1d);
            //_msPlot.PlotHistogram(noid, "noId ions", axislabel, 1d);

            //_msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true,5);
            //_msPlot.FigureTitle(title + " (" + percentage.ToString() + "%)");
            _msPlot.SaveCurrentPic(_outputdir + title + ".png");
        }

        private void normalizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var count = new List<int>();

            var rt = Parameter.ElutingRTMin;
            foreach (
                var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(0, Parameter.RT_max_cutoff, rt)
                )
            {
                if (pep.Charge == 1)
                    continue;
                if (!_projectManager.CurrentProject.FeatureManager.IsAnalyzed(pep))
                    continue;
                count.Add(_projectManager.CurrentProject.FeatureManager.FeatureToScans(pep).Count);
            }
            _msPlot.PlotFeatureAnalyzeTimes(count, _projectManager.CurrentProject.ID, true);

            //_msPlot.PlotHistogram()
        }

        private void distributionInIsolationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filename = "";
            var axislabel = "Diff. to center of isolation window";
            var ylabel = "count of precursor ions";
            var interval = 0.02d;

            #region different charge

            for (var i = 1; i <= 5; i++)
            {
                var title = _projectManager.CurrentProject.ID + "_Z" + i;
                //string title = _projectManager.CurrentProject.ID;
                var idions = new List<double>();
                var noid = new List<double>();
                foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                {
                    if (scan.RetentionTime < Parameter.RT_min_cutoff || scan.RetentionTime > Parameter.RT_max_cutoff)
                        continue;
                    foreach (var ion in scan.CoIsolatedIons)
                    {
                        if (ion.Charge != i)
                            continue;
                        var delta = ion.Mz - scan.TargetPrecursorMz;
                        if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
                            idions.Add(delta);
                        else
                            noid.Add(delta);
                    }
                }
                var percentage = ((double) idions.Count/(idions.Count + noid.Count));
                percentage = Math.Round(percentage, 4)*100;
                _msPlot.ClearCurves();


                _msPlot.PlotAppendHistogram(idions, "Id ions", axislabel, ylabel, interval);
                _msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, interval);
                _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 100);
                _msPlot.ChartXAxisRange(-1.1d, 1.1d);
                _msPlot.FigureTitle(title + " (" + percentage + "%)");
                _msPlot.SaveCurrentPic(_outputdir + title + ".png");


                var sw = new StreamWriter(_outputdir + title);

                var max = idions.Count;
                if (max < noid.Count)
                    max = noid.Count;
                for (var idx = 0; idx < max; idx++)
                {
                    if (idions.Count > idx)
                    {
                        sw.Write(idions[idx] + "\t");
                    }
                    else
                        sw.Write("\t");
                    if (noid.Count > idx)
                    {
                        sw.Write(noid[idx]);
                    }
                    sw.WriteLine();
                }
                sw.Close();
            }

            #endregion

            //List<double> IDions = new List<double>();
            //List<double> noID = new List<double>();
            //foreach (IsolatedRegion scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            //{
            //    foreach (PrecursorIon ion in scan.CoIsolatedIons)
            //    {
            //        double delta = ion.Mz - scan.TargetPrecursorMz;

            //        if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
            //            IDions.Add(delta);
            //        else
            //            noID.Add(delta);
            //    }
            //}
            //double per = ((double)IDions.Count / (IDions.Count + noID.Count));


            //per = Math.Round(per, 4) * 100;
            //_msPlot.ClearCurves();
            //string pictitle = _projectManager.CurrentProject.ID;
            //_msPlot.PlotAppendHistogram(IDions, "Id ions", axislabel, ylabel, interval);
            //_msPlot.PlotAppendHistogram(noID, "noId ions", axislabel, ylabel, interval);
            //_msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true);
            //_msPlot.FigureTitle(pictitle + " (" + per.ToString() + "%)");
            ////_msPlot.ChartXAxisRange(-1.5d, 1.5d);

            //_msPlot.SaveCurrentPic(_outputdir + pictitle + ".png");
        }

        private void onlyTargetedIonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var values = new List<double>();
            var axislabel = "Diff. to center of isolation window";
            var ylabel = "count of precursor ions";
            if (_projectManager.CurrentProject.RawInfo == null)
                return;
            var scanMap = new Dictionary<int, ScanTitle>();
            foreach (var scan in _projectManager.CurrentProject.RawInfo.Scanlist)
            {
                if (scan.ScanOrder != 2)
                    continue;
                scanMap.Add(scan.ScanNo, scan);
            }

            for (var i = 1; i <= 5; i++)
            {
                var title = _projectManager.CurrentProject.ID + "_onlyTarget_Z" + i;
                //string title = _projectManager.CurrentProject.ID;
                var idions = new List<double>();
                var noid = new List<double>();

                foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                {
                    var pep = _projectManager.CurrentProject.FeatureManager.TargetFeature(scan);
                    if (pep.Charge != i)
                        continue;
                    // only count the scans where the kronik and raw file agree on charge states
                    if (pep.Charge != scanMap[scan.ScanNo].Charge)
                        continue;
                    var delta = pep.BaseMz - scan.TargetPrecursorMz;
                    if (_projectManager.CurrentProject.FeatureManager.IsIdentified(pep))
                        idions.Add(delta);
                    else
                        noid.Add(delta);
                }

                var percentage = ((double) idions.Count/(idions.Count + noid.Count));

                percentage = Math.Round(percentage, 4)*100;
                _msPlot.ClearCurves();
                var interval = 0.02d;
                _msPlot.PlotAppendHistogram(idions, "Id ions", axislabel, ylabel, interval);
                _msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, interval);
                _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 10);
                _msPlot.FigureTitle(title + " (" + percentage + "%)");
                _msPlot.SaveCurrentPic(_outputdir + title + ".png");
            }
        }

        private void onlyTargetedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _msPlot.ClearCurves();
            if (_projectManager.CurrentProject == null)
                return;

            var idions = new List<double>();
            var noid = new List<double>();
            var count = 0;
            var scanCount = 0;
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                scanCount++;
                var rt = scan.RetentionTime;
                var ion = scan.TargetIon;
                var pep = _projectManager.CurrentProject.FeatureManager.IonToFeature(ion);
                if (pep.RT[1] < rt || pep.RT[0] > rt)
                {
                    count++;
                    continue;
                }
                var Ttime = pep.RT[1] - pep.RT[0];
                var normalizedRT = (rt - pep.RT[0])/Ttime;
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
                    idions.Add(normalizedRT);
                else
                    noid.Add(normalizedRT);
            }
            var ylabel = "count of precursor ions";
            var axislabel = "Reative Location (Normalized to feature width)";

            _msPlot.PlotAppendHistogram(idions, "Id ions", axislabel, ylabel, 0.01d);
            _msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, 0.01d);
            _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_samplingDistribution.png");
            //foreach (IsolatedRegion scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())

            Console.WriteLine("Count:" + count + "\tScanCount:" + scanCount);
        }

        private void distributionAmongFeatureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var idions = new List<double>();
            var noid = new List<double>();
            var count = 0;
            var scanCount = 0;
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                scanCount++;
                var rt = scan.RetentionTime;
                foreach (var ion in scan.CoIsolatedIons)
                {
                    var pep = _projectManager.CurrentProject.FeatureManager.IonToFeature(ion);
                    // remove the late eluted peptide freature from our analysis
                    if (pep.RT[0] <= Parameter.RT_max_cutoff)
                        continue;
                    if (pep.RT[1] < rt || pep.RT[0] > rt)
                    {
                        count++;
                        continue;
                    }

                    var Ttime = pep.RT[1] - pep.RT[0];
                    var normalizedRT = (rt - pep.RT[0])/Ttime;
                    if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
                        idions.Add(normalizedRT);
                    else
                        noid.Add(normalizedRT);
                }
            }
            var ylabel = "count of precursor ions";
            var axislabel = "Reative Location (Normalized to feature width)";

            _msPlot.ClearCurves();
            _msPlot.PlotAppendHistogram(idions, "Id ions", axislabel, ylabel, 0.01d);
            _msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, 0.01d);
            _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_samplingDistribution.png");
            //foreach (IsolatedRegion scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            var path = _outputdir + _projectManager.CurrentProject.ID + "_samplingDistribution";
            var sw = new StreamWriter(path);

            var max = idions.Count;
            if (max < noid.Count)
                max = noid.Count;
            for (var idx = 0; idx < max; idx++)
            {
                if (idions.Count > idx)
                    sw.Write(idions[idx] + "\t");
                else
                    sw.Write("\t");

                if (noid.Count > idx)
                {
                    sw.Write(noid[idx]);
                }
                sw.WriteLine();
            }
            sw.Close();

            Console.WriteLine("Count:" + count + "\tScanCount:" + scanCount);
        }

        private void baseOnRawFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var counts = new Dictionary<int, int>();
            var scanMap = new Dictionary<int, IsolatedRegion>();

            if (_projectManager.CurrentProject.RawInfo != null)
            {
                foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                {
                    scanMap.Add(scan.ScanNo, scan);
                }

                for (var charge = 1; charge <= 5; charge++)
                {
                    var total = 0d;
                    counts = new Dictionary<int, int>();
                    foreach (var rawscan in _projectManager.CurrentProject.RawInfo.Scanlist)
                    {
                        // not in ms2 file
                        if (!scanMap.ContainsKey(rawscan.ScanNo))
                            continue;
                        if (rawscan.Charge != charge)
                            continue;

                        var pep =
                            _projectManager.CurrentProject.FeatureManager.TargetFeature(scanMap[rawscan.ScanNo]);
                        if (!counts.ContainsKey(pep.Charge))
                            counts.Add(pep.Charge, 0);
                        counts[pep.Charge]++;
                        total += 1;
                    }
                    _msPlot.ClearCurves();

                    foreach (var z in counts.Keys)
                    {
                        var precentage = Math.Round(counts[z]/total, 4)*100;
                        _msPlot.PlotPieChart(z + "(" + precentage + "%)", counts[z]);
                    }
                    var title = _projectManager.CurrentProject.ID + "_RawChargeState" + charge;
                    _msPlot.FigureTitle(title);
                    _msPlot.FigureLegand("Total number of scans: " + ((int) total));
                    _msPlot.SaveCurrentPic(_outputdir + title + ".png");
                }
            }
        }

        private void baseOnKronikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var scanMap = new Dictionary<int, IsolatedRegion>();
            if (_projectManager.CurrentProject.RawInfo != null)
            {
                for (var charge = 1; charge <= 5; charge++)
                {
                    var counts = new Dictionary<int, int>();
                    var total = 0d;
                    foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                    {
                        var pep = _projectManager.CurrentProject.FeatureManager.TargetFeature(scan);
                        if (pep.Charge != charge)
                            continue;
                        scanMap.Add(scan.ScanNo, scan);
                    }
                    foreach (var rawscan in _projectManager.CurrentProject.RawInfo.Scanlist)
                    {
                        // not in ms2 file
                        if (!scanMap.ContainsKey(rawscan.ScanNo))
                            continue;
                        if (!counts.ContainsKey(rawscan.Charge))
                            counts.Add(rawscan.Charge, 0);
                        total += 1;
                        counts[rawscan.Charge]++;
                    }
                    _msPlot.ClearCurves();


                    foreach (var z in counts.Keys)
                    {
                        var precentage = Math.Round(counts[z]/total, 4)*100;
                        _msPlot.PlotPieChart(z + "(" + precentage + "%)", counts[z]);
                    }
                    var title = _projectManager.CurrentProject.ID + "_KronikChargeState" + charge;
                    _msPlot.FigureTitle(title);
                    _msPlot.FigureLegand("Total number of scans: " + ((int) total));
                    _msPlot.SaveCurrentPic(_outputdir + title + ".png");
                }
            }
        }

        private void outputToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null || _projectManager.CurrentProject.RawInfo == null)
                return;
            var scanMap = new Dictionary<int, IsolatedRegion>();
            var kronikCount = new Dictionary<int, int>();
            var rawCount = new Dictionary<int, int>();
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                var pep = _projectManager.CurrentProject.FeatureManager.TargetFeature(scan);
                scanMap.Add(scan.ScanNo, scan);
                if (!kronikCount.ContainsKey(pep.Charge))
                    kronikCount.Add(pep.Charge, 0);
                kronikCount[pep.Charge]++;
            }

            foreach (var rawscan in _projectManager.CurrentProject.RawInfo.Scanlist)
            {
                if (!scanMap.ContainsKey(rawscan.ScanNo))
                    continue;
                if (!rawCount.ContainsKey(rawscan.Charge))
                    rawCount.Add(rawscan.Charge, 0);
                rawCount[rawscan.Charge]++;
            }

            var path = _outputdir + _projectManager.CurrentProject.ID + "_chargeCount_2";
            var sw = new StreamWriter(path);
            var ksb = new StringBuilder();
            var rsb = new StringBuilder();
            ksb.Append("Kronik");
            rsb.Append("Raw");
            for (var i = 1; i <= 6; i ++)
            {
                ksb.Append("\t");
                rsb.Append("\t");
                if (kronikCount.ContainsKey(i))
                    ksb.Append(kronikCount[i].ToString());
                if (rawCount.ContainsKey(i))
                    rsb.Append(rawCount[i].ToString());
            }
            sw.WriteLine(rsb.ToString());
            sw.WriteLine(ksb.ToString());
            sw.Close();
        }

        private void compareTwoProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _msPlot.ClearCurves();
            foreach (var project in _projectManager.AllProjects)
            {
                var idions = new List<double>();
                var noid = new List<double>();
                var totalions = new List<double>();

                foreach (var scan in project.FeatureManager.IsolatedRegions())
                {
                    foreach (var ion in scan.CoIsolatedIons)
                    {
                        if (ion.Intensity == 0)
                            continue;

                        if (project.FeatureManager.IsIdentified(ion))
                            idions.Add(Math.Log10(ion.Intensity));
                        else
                            noid.Add(Math.Log10(ion.Intensity));
                    }
                }
                totalions.AddRange(idions);
                totalions.AddRange(noid);

                //_msPlot.PlotHistogram(idions, project.ID, "Intensity (log scale)", 0.1d);
                //_msPlot.PlotHistogram(totalions, project.ID, "Intensity (log scale)", 0.1d);
                _msPlot.PlotHistogram(noid, project.ID, "Intensity (log scale)", 0.1d);
            }
            _msPlot.FigureTitle("Precursor ion distribution");
            _msPlot.SaveCurrentPic(_outputdir + "Precursor_Ion_Distribution.png");
        }

        private void precursorIonsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var idions = new List<double>();
            var noid = new List<double>();

            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                // do not count the ms2 scans after certain time point
                if (scan.RetentionTime > Parameter.RT_max_cutoff || scan.RetentionTime < Parameter.RT_min_cutoff)
                    continue;

                foreach (var ion in scan.CoIsolatedIons)
                {
                    if (ion.Intensity == 0)
                        continue;
                    if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
                        idions.Add(Math.Log10(ion.Intensity));
                    else
                        noid.Add(Math.Log10(ion.Intensity));
                }
            }
            var total = idions.Count + noid.Count;

            var ylabel = "count of precursor ions";
            var axislabel = "Intensity (log scale)";

            var interval = 0.2d;
            _msPlot.ClearCurves();
            _msPlot.PlotHistogram(idions, "Id ions", ylabel, axislabel, interval, false);
            _msPlot.PlotHistogram(noid, "noId ions", ylabel, axislabel, interval, false);
            _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
            _msPlot.ChartYAxisRange(0, 20000d);
            _msPlot.ChartXAxisRange(3, 8);
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_intensityID.png");


            var filepath = _outputdir + _projectManager.CurrentProject.ID + "_intensityID";
            var sw = new StreamWriter(filepath);
            var max = idions.Count;
            if (max < noid.Count)
                max = noid.Count;
            for (var idx = 0; idx < max; idx++)
            {
                if (idions.Count > idx)
                    sw.Write(idions[idx] + "\t");
                else
                    sw.Write("\t");

                if (noid.Count > idx)
                {
                    sw.Write(noid[idx]);
                }
                sw.WriteLine();
            }
            sw.Close();

            //Console.WriteLine("Count:" + count.ToString() + "\tScanCount:" + scanCount.ToString());


            Console.WriteLine("nubmer of used ion in this plot: " + total);
        }

        private void peptdieFeaturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var rt = Parameter.ElutingRTMin;
            var id = new List<double>();
            var noid = new List<double>();
            foreach (
                var pep in
                    _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(0, Parameter.RT_max_cutoff, rt))
            {
                var value = Math.Log10(pep.SummedIntensity);
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(pep))
                    id.Add(value);
                else
                    noid.Add(value);
                //pep.SummedIntensity
            }

            Console.WriteLine("\nID:" + id.Count);
            Console.WriteLine("NoID:" + noid.Count);

            var ylabel = "Count of peptide features";
            var axislabel = "Intensity (log scale)";
            var percentage = Math.Round((double) id.Count/(id.Count + noid.Count), 4)*100;

            _msPlot.ClearCurves();
            var interval = 0.2d;
            _msPlot.PlotHistogram(id, "Id ions", ylabel, axislabel, interval, false);
            _msPlot.PlotHistogram(noid, "noId ions", ylabel, axislabel, interval, false);

            //_msPlot.PlotAppendHistogram(id, "Id ions", axislabel, ylabel, 0.1d);
            //_msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, 0.1d);
            _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            _msPlot.ChartYAxisRange(0, 3500d);
            _msPlot.ChartXAxisRange(3, 11);


            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_PepIntID.png");


            var filepath = _outputdir + _projectManager.CurrentProject.ID + "_PepIntID";
            var sw = new StreamWriter(filepath);
            var max = id.Count;
            if (max < noid.Count)
                max = noid.Count;
            for (var idx = 0; idx < max; idx++)
            {
                if (id.Count > idx)
                    sw.Write(id[idx] + "\t");
                else
                    sw.Write("\t");

                if (noid.Count > idx)
                {
                    sw.Write(noid[idx]);
                }
                sw.WriteLine();
            }
            sw.Close();
            //Console.WriteLine("nubmer of used ion in this plot: " + total.ToString());
        }

        private void chargesOnMS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null || _projectManager.CurrentProject.RawInfo == null ||
                _projectManager.CurrentProject.FeatureManager.SearchResult == null)
                return;


            var id = new List<double>();
            var noid = new List<double>();
            var raw2ms2 = new Dictionary<int, IsolatedRegion>();
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                raw2ms2.Add(scan.ScanNo, scan);
            }

            var idcount = 0d;
            var noidcount = 0d;
            foreach (var rawscan in _projectManager.CurrentProject.RawInfo.Scanlist)
            {
                if (rawscan.RetentionTime > Parameter.RT_max_cutoff || rawscan.RetentionTime < Parameter.RT_min_cutoff)
                    continue;
                if (!raw2ms2.ContainsKey(rawscan.ScanNo))
                    continue;
                var ions = Math.Log10(rawscan.Ions);
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(raw2ms2[rawscan.ScanNo]))
                    id.Add(ions);
                else
                    noid.Add(ions);
                if (ions > 5.4 && ions < 5.6)
                {
                    if (_projectManager.CurrentProject.FeatureManager.IsIdentified(raw2ms2[rawscan.ScanNo]))
                        idcount++;
                    else
                        noidcount++;
                }
            }


            var ylabel = "# of MS2 scans";
            var axislabel = "Charges on MS2";
            var percentage = Math.Round((double) id.Count/(id.Count + noid.Count), 4)*100;

            _msPlot.ClearCurves();
            var interval = 0.1d;
            _msPlot.PlotHistogram(id, "ID scans", ylabel, axislabel, interval, false);
            _msPlot.PlotHistogram(noid, "noID scans", ylabel, axislabel, interval, false);

            //_msPlot.PlotAppendHistogram(id, "Id ions", axislabel, ylabel, 0.1d);
            //_msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, 0.1d);
            _msPlot.ComputePercentage("noID scans", "ID scans", "percentage (ID/All)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            _msPlot.ChartXAxisRange(3, 7);
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_ChargesID.png");

            //Console.WriteLine("nubmer of used ion in this plot: " + total.ToString());
        }

        private void outputIdentificationComparisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filepath = _outputdir + "PeptideIDComparision";

            var searchresults = new List<Percolator>();
            foreach (var project in _projectManager.AllProjects)
            {
                if (project.FeatureManager.SearchResult != null)
                    searchresults.Add(project.FeatureManager.SearchResult);
            }
            if (searchresults.Count > 0)
                Output.ComparePepIDResults(filepath, searchresults);
            else
                MessageBox.Show("no search results loaded.");

            #region only need search result

            //OpenFileDialog op = new OpenFileDialog();
            //op.Filter = "*.perc.xml (Percolator result)|*.xml";
            //op.Multiselect = true;
            //Dictionary<string, Percolator> allresults = new Dictionary<string, Percolator>();

            //if (op.ShowDialog() == DialogResult.OK)
            //{
            //    foreach (string filepath in op.FileNames)
            //    {
            //        string id = Path.GetFileNameWithoutExtension(filepath);
            //        allresults.Add(id, new Percolator(filepath, id));
            //    }
            //}
            //string outfile = _outputdir + @"\Comparison_Result";
            //Output.ComparePercolatorResults(outfile, allresults);

            #endregion
        }

        private void precursorVsIdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var idScans = new List<double>();
            var noid = new List<double>();

            //List<IsolatedRegion> scans = FindQualifiedMS2Region(_charge_min, _charge_max, _projectManager.CurrentProject);

            //foreach (IsolatedRegion scan in scans)
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(scan))
                    idScans.Add(scan.CoIsolatedIons.Count);
                else
                    noid.Add(scan.CoIsolatedIons.Count);
            }

            double total = idScans.Count + noid.Count;
            var percentage = Math.Round(idScans.Count/total, 4)*100;
            var ylabel = "count of scans";
            var axislabel = "# precursor ions in isolation window";

            //_msPlot.PlotHistogram(idScans, "Id scans", ylabel, axislabel, 1, false);
            //_msPlot.PlotHistogram(noid, "noId scans", ylabel, axislabel, 1, false);
            _msPlot.PlotAppendHistogram(idScans, "Id scans", axislabel, ylabel, 1);
            _msPlot.PlotAppendHistogram(noid, "noId scans", axislabel, ylabel, 1);


            _msPlot.ComputePercentage("noId scans", "Id scans", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_coIsolation.png");
        }

        private void ratiosVsIdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var idScans = new List<double>();
            var noid = new List<double>();


            //List<IsolatedRegion> scans = FindQualifiedMS2Region(_charge_min, _charge_max, _projectManager.CurrentProject);

            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                //foreach (IsolatedRegion scan in scans)
            {
                var intensities = new List<double>();
                foreach (var ion in scan.CoIsolatedIons)
                {
                    if (ion.Intensity == 0)
                        continue;
                    intensities.Add(ion.Intensity);
                }
                intensities.Sort();

                var ratio = 0d;
                if (intensities.Count > 1)
                {
                    if (intensities[intensities.Count - 1] > 0 && intensities[intensities.Count - 2] > 0)
                        ratio = Math.Round(intensities[intensities.Count - 2]/intensities[intensities.Count - 1], 2);
                }
                else if (intensities.Count == 1)
                    ratio = 1d;
                else
                    ratio = 0;


                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(scan))
                    idScans.Add(ratio);
                else
                    noid.Add(ratio);
            }

            double total = idScans.Count + noid.Count;
            var percentage = Math.Round(idScans.Count/total, 4)*100;
            var ylabel = "count of scans";
            var axislabel = "precursor intensity ratio: 2ed/1st";
            var interval = 0.1;
            _msPlot.PlotHistogram(idScans, "Id scans", ylabel, axislabel, interval, false);
            _msPlot.PlotHistogram(noid, "noId scans", ylabel, axislabel, interval, false);
            _msPlot.ComputePercentage("noId scans", "Id scans", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_precRatio.png");
        }

        private void identificationOverTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null ||
                _projectManager.CurrentProject.FeatureManager.SearchResult == null)
                return;

            var id = new List<double>();
            var noid = new List<double>();

            foreach (var scan in _projectManager.CurrentProject.FeatureManager.GetSortedScans())
            {
                //if (scan.RetentionTime > Parameter.RT_max_cutoff)
                //    continue;
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(scan))
                    id.Add(scan.RetentionTime);
                else
                    noid.Add(scan.RetentionTime);
            }
            var ylabel = "Number of peptide features";
            var axislabel = "RT";
            var percentage = Math.Round((double) id.Count/(id.Count + noid.Count), 4)*100;
            string outfilepaht = _outputdir + @"/" + _projectManager.CurrentProject.ID + "_id_ms2_overtime.csv";
            Dictionary<string, List<double>> dataseries = new Dictionary<string, List<double>>();
            dataseries.Add("No ID MS2", noid);
            dataseries.Add("ID MS2", id);
            Output.OuptputDataSeies(dataseries, outfilepaht);


            _msPlot.ClearCurves();
            _msPlot.PlotAppendHistogram(id, "Id scans", axislabel, ylabel, 1d);
            _msPlot.PlotAppendHistogram(noid, "noId scans", axislabel, ylabel, 1d);
            _msPlot.ComputePercentage("noId scans", "Id scans", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_ScanID_RT.png");
        }

        private void featureIntensityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var values = new List<double>();
            var rt = Parameter.ElutingRTMin;
            foreach (
                var pep in
                    _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(0, Parameter.RT_max_cutoff, rt))
            {
                var intensity = Math.Log10(pep.SummedIntensity);
                values.Add(intensity);
            }

            var ylabel = "# of peptide features";
            var axislabel = "Intensity (log scale)";
            //double percentage = Math.Round((double)id.Count / (id.Count + noid.Count), 4) * 100;
            var interval = 0.2d;
            _msPlot.PlotHistogram(values, _projectManager.CurrentProject.ID + "_" + values.Count, ylabel, axislabel,
                interval, false);
            _msPlot.FigureTitle("Peptide Feature Intensity Distribution");

            //_msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_ScanID_RT.png");
        }

        private void abundanceDistributionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null || _projectManager.CurrentProject.RawInfo == null)
                return;

            var values = new List<double>();

            foreach (var scan in _projectManager.CurrentProject.RawInfo.Scanlist)
            {
                if (scan.RetentionTime > Parameter.RT_max_cutoff)
                    continue;

                var intensity = Math.Log10(scan.Ions);
                values.Add(intensity);
            }

            var ylabel = "# of MS2 scans ";
            var axislabel = "Charges (log scale)";
            //double percentage = Math.Round((double)id.Count / (id.Count + noid.Count), 4) * 100;
            var interval = 0.1d;
            _msPlot.PlotHistogram(values, _projectManager.CurrentProject.ID + "_" + values.Count, ylabel, axislabel,
                interval, false);
            _msPlot.FigureTitle("MS2 scan Charges Distribution");
        }

        private void iTVsChargeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null || _projectManager.CurrentProject.RawInfo == null)
                return;
            var ITs = new List<double>();
            var Charges = new List<double>();

            foreach (var scan in _projectManager.CurrentProject.RawInfo.Scanlist)
            {
                if (scan.ScanOrder != 2)
                    continue;
                if (scan.RetentionTime > Parameter.RT_max_cutoff)
                {
                    ITs.Add(scan.IonInjectionTime);
                    var ion = Math.Log10(scan.Ions);
                    Charges.Add(ion);
                }
            }
            var yfactor = 0.01;
            var xfactor = 0.01;
            var xlabel = "IT";
            var ylabel = "Ions";
            _msPlot.PlotScotterPlot(Charges, ITs, ylabel, xlabel, yfactor, xfactor);
        }

        private void outputPeptideFeatureAbundanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Output.OutputIDFeatureAbundance(_outputdir + "IDPeptideAbundance", _projectManager);
        }

        private void differentChargesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var idions = new List<double>();
            var noid = new List<double>();

            for (var z = 2; z <= 5; z++)
            {
                foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                {
                    // do not count the ms2 scans after certain time point
                    //if (scan.RetentionTime > Parameter.RT_max_cutoff)
                    //    continue;

                    foreach (var ion in scan.CoIsolatedIons)
                    {
                        if (ion.Intensity == 0)
                            continue;
                        if (ion.Charge != z)
                            continue;
                        if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
                            idions.Add(Math.Log10(ion.Intensity));
                        else
                            noid.Add(Math.Log10(ion.Intensity));
                    }
                }
                var total = idions.Count + noid.Count;

                var ylabel = "count of precursor ions";
                var axislabel = "Intensity (log scale)";

                var interval = 0.2d;
                _msPlot.ClearCurves();
                _msPlot.PlotHistogram(idions, "Id ions", ylabel, axislabel, interval, false);
                _msPlot.PlotHistogram(noid, "noId ions", ylabel, axislabel, interval, false);
                _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
                var title = _projectManager.CurrentProject.ID + "_intensityID_" + z;
                _msPlot.FigureTitle(title);
                _msPlot.SaveCurrentPic(_outputdir + title + ".png");
                Console.WriteLine("nubmer of used ion in this plot: " + total);
            }
        }

        private void featureIntensityVsSpectraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var sw = new StreamWriter(_outputdir + _projectManager.CurrentProject.ID + "_FeatureInt_samplingTimes");


            // convert to percentage
            var rawValues = new List<double>();
            var rt = Parameter.ElutingRTMin;
            foreach (
                var pep in
                    _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(0, Parameter.RT_max_cutoff, rt))
            {
                rawValues.Add(pep.SummedIntensity);
            }
            rawValues.Sort();
            var intToPercentage = new Dictionary<double, double>();

            double total = rawValues.Count;
            for (var i = 0; i < rawValues.Count; i++)
            {
                var percentage = Math.Round((i + 1)/total, 4)*100;
                if (intToPercentage.ContainsKey(rawValues[i]))
                    intToPercentage[rawValues[i]] = percentage;
                else
                    intToPercentage.Add(rawValues[i], percentage);

                if (percentage > 100 || percentage == 0)
                    Console.WriteLine(percentage.ToString());
            }
            // end of region

            var xvalue = new List<double>();

            foreach (
                var pep in
                    _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(0, Parameter.RT_max_cutoff, rt))
            {
                //double value = Math.Log10(pep.SummedIntensity);
                var value = intToPercentage[pep.SummedIntensity];
                sw.WriteLine(value + "\t" + _projectManager.CurrentProject.FeatureManager.FeatureToScans(pep).Count);

                for (var i = 0; i < _projectManager.CurrentProject.FeatureManager.FeatureToScans(pep).Count; i++)
                {
                    xvalue.Add(value);
                }
            }

            sw.Close();


            //string ylabel = "# MS2 spectra";
            var ylabel = "Precentage";
            var axislabel = "feature bin by intensity";

            //_msPlot.ClearCurves();
            var interval = 5d;
            _msPlot.PlotHistogram(xvalue, _projectManager.CurrentProject.ID, ylabel, axislabel, interval, false);
            //_msPlot.PlotHistogram(noid, "noId ions", ylabel, axislabel, interval, false);
            var title = _projectManager.CurrentProject.ID;
            _msPlot.FigureTitle(title);


            //_msPlot.PlotAppendHistogram(id, "Id ions", axislabel, ylabel, 0.1d);
            //_msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, 0.1d);
            //_msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle("");
            _msPlot.SaveCurrentPic(_outputdir + title + ".png");
        }

        private void differentChargesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var rt = Parameter.ElutingRTMin;
            for (var z = 2; z <= 5; z++)
            {
                var id = new List<double>();
                var noid = new List<double>();
                foreach (
                    var pep in
                        _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(0, Parameter.RT_max_cutoff, rt))
                {
                    if (pep.Charge != z)
                        continue;
                    var value = Math.Log10(pep.SummedIntensity);
                    if (_projectManager.CurrentProject.FeatureManager.IsIdentified(pep))
                        id.Add(value);
                    else
                        noid.Add(value);
                    //pep.SummedIntensity
                }

                Console.WriteLine("\nID:" + id.Count);
                Console.WriteLine("NoID:" + noid.Count);

                var ylabel = "Count of peptide features";
                var axislabel = "Intensity (log scale)";
                var percentage = Math.Round((double) id.Count/(id.Count + noid.Count), 4)*100;

                _msPlot.ClearCurves();
                var interval = 0.2d;
                _msPlot.PlotHistogram(id, "Id ions", ylabel, axislabel, interval, false);
                _msPlot.PlotHistogram(noid, "noId ions", ylabel, axislabel, interval, false);
                var title = _projectManager.CurrentProject.ID + "_PepIntID_" + z;
                _msPlot.FigureTitle(title);


                //_msPlot.PlotAppendHistogram(id, "Id ions", axislabel, ylabel, 0.1d);
                //_msPlot.PlotAppendHistogram(noid, "noId ions", axislabel, ylabel, 0.1d);
                _msPlot.ComputePercentage("noId ions", "Id ions", "percentage (Id/all)", true, 0);
                _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
                _msPlot.SaveCurrentPic(_outputdir + title + ".png");
            }
        }

        private void scansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var values = new List<double>();
            var noid = new List<double>();
            foreach (var ms2 in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                if (!_projectManager.CurrentProject.FeatureManager.IsIdentified(ms2))
                    noid.Add(ms2.TargetPrecursorMz);
                else
                    values.Add(ms2.TargetPrecursorMz);
            }

            var ylabel = "";
            var axislabel = "";

            //_msPlot.ClearCurves();
            var interval = 10d;
            //_msPlot.PlotHistogram(values, _projectManager.CurrentProject.ID, ylabel, axislabel, interval, false);
            _msPlot.PlotHistogram(values, "id", ylabel, axislabel, interval, false);
            _msPlot.PlotHistogram(noid, "no id", ylabel, axislabel, interval, false);
            var title = _projectManager.CurrentProject.ID + "_id scans across mz";
            _msPlot.ComputePercentage("no id", "id", "percentage (Id/all)", true, 0);

            _msPlot.FigureTitle(title);
        }

        private void peptidesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var mzToPeptide = new Dictionary<double, List<string>>();


            foreach (var ms2 in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                foreach (var psm in _projectManager.CurrentProject.FeatureManager.IdPSMs(ms2))
                {
                    if (!mzToPeptide.ContainsKey(ms2.TargetPrecursorMz))
                        mzToPeptide.Add(ms2.TargetPrecursorMz, new List<string>());
                    mzToPeptide[ms2.TargetPrecursorMz].Add(psm.Sequence);
                }
            }
            var ylabel = "# peptides";
            var axislabel = "m/z";

            //_msPlot.ClearCurves();
            var interval = 10d;
            _msPlot.PlotHistogramCountByUniqueValue(mzToPeptide, _projectManager.CurrentProject.ID, axislabel, ylabel,
                interval);
            var title = _projectManager.CurrentProject.ID + "_id peptides across mz";

            _msPlot.FigureTitle(title);
        }

        private void proteinsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var mzToPeptide = new Dictionary<double, List<string>>();


            foreach (var ms2 in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                foreach (var psm in _projectManager.CurrentProject.FeatureManager.IdPSMs(ms2))
                {
                    if (!mzToPeptide.ContainsKey(ms2.TargetPrecursorMz))
                        mzToPeptide.Add(ms2.TargetPrecursorMz, new List<string>());
                    foreach (var protein in psm.proteins)
                    {
                        mzToPeptide[ms2.TargetPrecursorMz].Add(protein.ID);
                    }
                }
            }
            var ylabel = "# protein";
            var axislabel = "m/z";

            //_msPlot.ClearCurves();
            var interval = 1d;
            _msPlot.PlotHistogramCountByUniqueValue(mzToPeptide, _projectManager.CurrentProject.ID, axislabel, ylabel,
                interval);
            var title = _projectManager.CurrentProject.ID + "_id proteins across mz";
            _msPlot.FigureTitle(title);
        }

        private void allPrecursorIntensityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var values = new List<double>();

            foreach (var scan in _projectManager.CurrentProject.FeatureManager.GetHardklor.Scans)
            {
                foreach (var ion in scan.Ionlist)
                {
                    //if (ion.Charge <= 1)
                    //    continue;

                    var value = Math.Log10(ion.Intensity);
                    if (value > 4)
                        continue;
                    values.Add(value);
                }
            }


            var ylabel = "Count of precursor ions";
            var axislabel = "Intensity (log scale)";

            var interval = 0.01d;
            _msPlot.PlotHistogram(values, _projectManager.CurrentProject.ID, ylabel, axislabel, interval, false);
            _msPlot.FigureTitle("Hardklor Peak Intensity Distribution");
            _msPlot.SaveCurrentPic(_outputdir + _projectManager.CurrentProject.ID + "_int.png");
        }

        private void featuresAndMS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var count = 0;
            // example -1
            //double maxMZ = 1236d;
            //double minMZ = 1232d;
            //double minRT = 40d;
            //double maxRT = 41d;

            // example-2
            //double maxMZ = 1220d;
            //double minMZ = 1190d;
            //double minRT = 39d;
            //double maxRT = 46d;

            //example -3
            //_maxMZ = 1200d;
            //_minMZ = 1100d;
            //_minRT = 10d;
            //_maxRT = 74d;

            //example -4
            //_maxMZ = 1180d;
            //_minMZ = 1176d;
            //_minRT = 66d;
            //_maxRT = 75d;

            _maxMZ = 800d;
            _minMZ = 500d;
            _minRT = 20d;
            _maxRT = 60d;


            var unAnalyzedFeature = new List<PepFeature>();
            var AnalyzedFeatures = new List<PepFeature>();
            foreach (var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                count++;
                var pepMZ = pep.BaseMz;
                if (pep.RT[0] > _maxRT || pep.RT[1] < _minRT || pepMZ < _minMZ || pepMZ > _maxMZ)
                    continue;
                if (!_projectManager.CurrentProject.FeatureManager.IsAnalyzed(pep))
                    unAnalyzedFeature.Add(pep);
                else
                    AnalyzedFeatures.Add(pep);
            }


            //List<IsolatedRegion> pepScans = _projectManager.CurrentProject.FeatureManager.FeatureToScans(unAnalyzedFeature[1]);


            // here is for debuging 
            var matched = new Dictionary<PepFeature, int>();
            var found = false;


            var matchedScans = new List<IsolatedRegion>();
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                var lowerMZ = scan.TargetPrecursorMz - 1;
                var higherMZ = scan.TargetPrecursorMz + 1;
                if (lowerMZ > _maxMZ || higherMZ < _minMZ || scan.RetentionTime > _maxRT || scan.RetentionTime < _minRT)
                    continue;
                matchedScans.Add(scan);

                // here is for debuging 
                foreach (var ion in scan.CoIsolatedIons)
                {
                    var pep = _projectManager.CurrentProject.FeatureManager.IonToFeature(ion);
                    if (!matched.ContainsKey(pep))
                        matched.Add(pep, 0);
                }
            }

            var noMatchedScans = new List<IsolatedRegion>();
            foreach (var notMatched in _projectManager.CurrentProject.NomatchedRegions())
            {
                var lowerMZ = notMatched.TargetPrecursorMz - 1;
                var higherMZ = notMatched.TargetPrecursorMz + 1;
                if (lowerMZ > _maxMZ || higherMZ < _minMZ || notMatched.RetentionTime > _maxRT ||
                    notMatched.RetentionTime < _minRT)
                    continue;
                noMatchedScans.Add(notMatched);
            }


            Console.WriteLine("total features: " + count);

            //_msPlot.plotAnalyzedRegion(matchedScans, _projectManager.CurrentProject.ID, Color.Red);
            _msPlot.twoDPlot(AnalyzedFeatures, _projectManager.CurrentProject.ID, Color.Blue, Color.CornflowerBlue);

            _msPlot.plotAnalyzedRegion(noMatchedScans, _projectManager.CurrentProject.ID, Color.DarkRed);
            _msPlot.twoDPlot(unAnalyzedFeature, _projectManager.CurrentProject.ID, Color.Chocolate, Color.BurlyWood);
        }

        private void mS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var selected = FindQualifiedMS2Region(_charge_min, _charge_max, _projectManager.CurrentProject);

            var id = new List<IsolatedRegion>();
            var noid = new List<IsolatedRegion>();

            foreach (var iso in selected)
            {
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(iso))
                {
                    id.Add(iso);
                }
                else
                    noid.Add(iso);
            }

            var xlabel = "m/z";
            var ylabel = "# ms2";
            double interval = 10;

            _msPlot.plotAnalyzedRegion(id, "id", Color.Blue);
            _msPlot.plotAnalyzedRegion(noid, "no id", Color.DarkRed);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
        }

        private void mzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selected = FindQualifiedMS2Region(_charge_min, _charge_max, _projectManager.CurrentProject);
            var id = new List<double>();
            var noid = new List<double>();

            foreach (var iso in selected)
            {
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(iso))
                {
                    id.Add(iso.TargetPrecursorMz);
                }
                else
                    noid.Add(iso.TargetPrecursorMz);
            }
            var xlabel = "m/z";
            var ylabel = "# ms2";
            double interval = 20;

            _msPlot.PlotHistogram(id, "id", ylabel, xlabel, interval, false);
            _msPlot.PlotHistogram(noid, "no id", ylabel, xlabel, interval, false);
            _msPlot.ComputePercentage("no id", "id", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
        }

        private void rTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selected = FindQualifiedMS2Region(_charge_min, _charge_max, _projectManager.CurrentProject);
            var id = new List<double>();
            var noid = new List<double>();

            foreach (var iso in selected)
            {
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(iso))
                {
                    id.Add(iso.RetentionTime);
                }
                else
                    noid.Add(iso.RetentionTime);
            }
            var xlabel = "RT";
            var ylabel = "# ms2";
            double interval = 2;

            _msPlot.PlotHistogram(id, "id", ylabel, xlabel, interval, false);
            _msPlot.PlotHistogram(noid, "no id", ylabel, xlabel, interval, false);
            _msPlot.ComputePercentage("no id", "id", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
        }

        private void chargeVsIdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            var selected = FindQualifiedMS2Region(_charge_min, _charge_max, _projectManager.CurrentProject);

            var id = new List<double>();
            var noid = new List<double>();
            foreach (var scan in selected)
            {
                foreach (var ion in scan.CoIsolatedIons)
                {
                    if (_projectManager.CurrentProject.FeatureManager.IsIdentified(ion))
                        id.Add(ion.Charge);
                    else
                        noid.Add(ion.Charge);
                }
            }

            var xlabel = "charge state";
            var ylabel = "# precursor ion";
            double interval = 1;

            _msPlot.PlotHistogram(id, "id", ylabel, xlabel, interval, false);
            _msPlot.PlotHistogram(noid, "no id", ylabel, xlabel, interval, false);
            _msPlot.ComputePercentage("no id", "id", "percentage (Id/all)", true, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
        }

        //private List<PrecursorIon> FindQualifiedPrecIon(double int_min, double int_max, Project project)
        //{

        //}

        //private List<IsolatedRegion> FindQualifiedMS2_PercInt(double int_min, double int_max, Project project)
        //{

        //}

        private List<IsolatedRegion> FindQualifiedMS2Region(double charge_min, double charge_max, Project project)
        {
            var scans = new List<ScanTitle>();
            foreach (var scan in project.RawInfo.Scanlist)
            {
                if (scan.RetentionTime > Parameter.RT_max_cutoff || scan.RetentionTime < Parameter.RT_min_cutoff)
                {
                    continue;
                }
                var logvalue = Math.Log10(scan.Ions);
                if (logvalue > charge_min && logvalue < charge_max)
                {
                    scans.Add(scan);
                }
                //project.FeatureManager.
            }

            var selected = new List<IsolatedRegion>();
            var i = 0;
            foreach (var iso in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                for (var idx = i; idx < scans.Count; idx++)
                {
                    if (scans[idx].ScanNo == iso.ScanNo)
                    {
                        selected.Add(iso);
                        break;
                    }
                    if (scans[idx].ScanNo > iso.ScanNo)
                    {
                        i = idx;
                        break;
                    }
                }
            }
            return selected;
        }

        private void quickTestKronikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;
            Parameter.RT_min_cutoff = 20;
            var qualifiedFeature = new List<double>();
            foreach (
                var feature in
                    _projectManager.CurrentProject.FeatureManager.PeptideFeatures(Parameter.RT_min_cutoff,
                        Parameter.RT_max_cutoff))
            {
                var pepRT = (feature.RT[1] - feature.RT[0])*60;

                var idx = (int) pepRT;
                for (var i = 0; i < idx; i++)
                {
                    if (i >= 10) // I only what to see 1 to 10 sec
                        break;
                    if (qualifiedFeature.Count < i + 1)
                    {
                        qualifiedFeature.Add(0);
                    }
                    qualifiedFeature[i]++;
                }
            }
            var xvalues = new List<double>();
            for (var i = 0; i < qualifiedFeature.Count; i++)
            {
                xvalues.Add(i + 1);
            }


            _msPlot.PlotBar(xvalues, qualifiedFeature, _projectManager.CurrentProject.ID, 1);
        }

        private void calculateAccuracyOfSamplingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filepath = AppDomain.CurrentDomain.BaseDirectory + @"/Accurcy";
            var sw = new StreamWriter(filepath);

            foreach (var project in _projectManager.AllProjects)
            {
                var unAnalyzedFeature = new List<PepFeature>();
                var AnalyzedFeatures = new List<PepFeature>();
                var count = 0;

                var tp = 0; // collect ms2 on feature
                var fn = 0; // feature without any ms2 scans
                var fp = 0; // collect ms2 on nothing
                foreach (
                    var pep in project.FeatureManager.PeptideFeatures(Parameter.RT_min_cutoff, Parameter.RT_max_cutoff))
                {
                    count++;
                    var pepMZ = pep.BaseMz;

                    //if (pep.RT[0] > Parameter.RT_max_cutoff || pep.RT[1] < Parameter.RT_min_cutoff)
                    //    continue;


                    if (!project.FeatureManager.IsAnalyzed(pep))
                    {
                        unAnalyzedFeature.Add(pep);
                        fn++;
                    }
                    else
                    {
                        tp ++;
                        AnalyzedFeatures.Add(pep);
                    }
                }


                fp = _projectManager.CurrentProject.NumboerOfNoMatchedMStwo;
                var sb = new StringBuilder();
                sb.AppendLine("Project:\t" + project.ID);
                sb.AppendLine("collect ms2 on feature (TP):\t" + tp);
                sb.AppendLine("collect ms2 on nothing (FP):\t" + fp);
                sb.AppendLine("feature without any ms2 scans (FN):\t" + fn);
                sb.AppendLine("total features: " + count);
                sw.WriteLine(sb.ToString());
                sw.WriteLine();
            }
            sw.Close();
        }

        private void dynamicRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void outputPrecursorCountDistributionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_projectManager.CurrentProject == null)
                return;

            var outfile = _outputdir + @"/" + _projectManager.CurrentProject.ID + "_precursor_count";
            Output.PrecursorCount(_projectManager.CurrentProject, outfile);
        }

        private void testToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var count = 0;
            double time = 0;
            foreach (var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(20d, 50d, 5d))
            {
                if (pep.Charge == 1)
                    continue;
                time += (pep.RT[1] - pep.RT[0])*60;
                count++;
            }

            Console.WriteLine(time/count);
        }

        private void curmulativeCurveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var interval = 0.01d;
            //foreach (Project project in _projectManager.AllProjects)
            //{
            var values = new List<double>();
            var analyzed = new List<double>();
            var id = new List<double>();
            foreach (var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                //if (pep.Charge == 1)
                //    continue;
                var width = pep.RT[1] - pep.RT[0];
                values.Add(width);
                if (_projectManager.CurrentProject.FeatureManager.IsAnalyzed(pep))
                    analyzed.Add(width);
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(pep))
                    id.Add(width);
            }
            _msPlot.PlotCurmulateCurve("All", values, interval);
            if (analyzed.Count > 0)
                _msPlot.PlotCurmulateCurve("Analyzed", analyzed, interval);
            if (id.Count > 0)
                _msPlot.PlotCurmulateCurve("ID", id, interval);
            //}
        }

        private void featureCountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var chargeDistribution = new Dictionary<int, int>();


            foreach (var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                if (!chargeDistribution.ContainsKey(pep.Charge))
                {
                    chargeDistribution.Add(pep.Charge, 0);
                }
                chargeDistribution[pep.Charge]++;
            }
            var multiple = 0;
            foreach (var z in chargeDistribution.Keys)
            {
                Console.WriteLine(z + "\t" + chargeDistribution[z]);
                if (z > 1)
                    multiple += chargeDistribution[z];
            }
            Console.WriteLine("feature with multiple charge: " + multiple);
        }

        private void count234PeptideFeatureOnlyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var qualified = 0;
            var ana = 0;
            var id = 0;
            var targeted = 0;
            foreach (var pep in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                if (pep.Charge > 4 || pep.Charge < 2)
                    continue;
                qualified ++;
                if (_projectManager.CurrentProject.FeatureManager.IsAnalyzed(pep))
                    ana++;
                if (_projectManager.CurrentProject.FeatureManager.IsIdentified(pep))
                    id++;
                if (_projectManager.CurrentProject.FeatureManager.IsTargeted(pep))
                    targeted++;
            }
            Console.WriteLine("Total Peptide feature: (+2,+3,+4)" + qualified);
            Console.WriteLine("Targeted Peptide feature: (+2,+3,+4)" + targeted);
            Console.WriteLine("Analyzed Peptide feature: (+2,+3,+4)" + ana);
            Console.WriteLine("ID Peptide feature:" + id);
        }

        #region output

        private void outputTargetFeatureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();

            if (_projectManager.CurrentProject != null)
            {
                var outfile = _outputdir + @"\target_features";
                Output.FlatPeptideFiles(outfile, _projectManager.CurrentProject.FeatureManager, false);
                sb.Append("output:\n" + outfile);
            }
            MessageBox.Show(sb.ToString());
        }

        private void outputIdFeaturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            if (_projectManager.CurrentProject != null)
            {
                var outfile = _outputdir + @"\id_features";
                Output.FlatPeptideFiles(outfile, _projectManager.CurrentProject.FeatureManager, true);
                sb.Append("output:\n" + outfile);
            }
            MessageBox.Show(sb.ToString());
        }


        private void outputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            if (_projectManager.CurrentProject != null)
            {
                var outfile = _outputdir + @"\RT_Int_TargetFeatures";
                Output.RTandIntensityOfTargetFeature(outfile, _projectManager.CurrentProject.FeatureManager);
                sb.Append("output:\n" + outfile);
            }
            MessageBox.Show(sb.ToString());
        }

        private void getUnMatchedMS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ms2file = @"O:\Lab\Data\130723_Yesat_RJ\hermie\Bullseye_1_30\Q_2012_0918_RJ_63.matches.ms2";
            GetPartMS2.UnIDMSSpectra(_projectManager.CurrentProject.FeatureManager, ms2file);
        }

        private void pIFToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var outfile = _outputdir + @"\PIF_Result";
            Output.PIFResult(outfile, _projectManager.CurrentProject.FeatureManager);
        }

        private void compareResultsnotRelatedToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void outputSampledPeptideFeaturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var outfile = _outputdir + @"PeptideAndItsScans";
            Output.PeptideAndSampledTimes(outfile, _projectManager.CurrentProject.FeatureManager);
        }

        #endregion

        #region Plots

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _msPlot.ClearCurves();
        }


        private void featureCountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NoiseDetection.DetectNoise(_projectManager.CurrentProject);


            return;
            double Int_interval = 100000;
            var rt_interval = 0.1d;
            double Int_slides = 10000;
            var rtIntCount = new Dictionary<double, List<double>>();

            var count = new Dictionary<double, int>();

            //foreach (PepFeature feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(0d, maxRT,rt))
            var maxRT = double.MinValue;
            var maxInt = double.MinValue;
            var minInt = double.MaxValue;
            var xvalue = new List<double>();

            foreach (var feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                //if (feature.Charge == 1)
                //    continue;
                var rt = (feature.RT[1] - feature.RT[0])*60;
                var tidx = Math.Ceiling(rt/rt_interval)*rt_interval;
                //double logvalue = Math.Log(feature.SummedIntensity, 2d);
                var intidx = Math.Ceiling(feature.SummedIntensity/Int_interval);
                //double intidx = Math.Ceiling(logvalue);s
                if (!rtIntCount.ContainsKey(tidx))
                    rtIntCount.Add(tidx, new List<double>());
                rtIntCount[tidx].Add(intidx);
                //double value = feature.SummedIntensity / time; 
                //double value = feature.SummedIntensity; 
                if (maxInt < intidx)
                    maxInt = intidx;
                if (minInt > intidx)
                    minInt = intidx;

                xvalue.Add(intidx);
            }
            var processed = new Dictionary<double, List<int>>();

            var interval = (maxInt - minInt)/Int_slides;
            var intBoundry = new List<double>();
            minInt += interval;
            for (var i = minInt; i <= maxInt; i += interval)
                intBoundry.Add(i);

            foreach (var rt in rtIntCount.Keys)
            {
                rtIntCount[rt].Sort();
                var cc = 0;
                var idx = 1;
                processed.Add(rt, new List<int>());
                for (var i = 0; i < rtIntCount[rt].Count; i++)
                {
                    if (rtIntCount[rt][i] <= intBoundry[idx])
                        cc++;
                    processed[rt].Add(cc);
                    cc = 0;
                    idx++;
                }
                processed[rt].Add(cc);
            }


            var rtidx = processed.Keys.ToList();
            rtidx.Sort();

            var sw = new StreamWriter(_outputdir + @"/feature_int_rt_distribution");

            for (var idx = 0; idx < rtidx.Count; idx++)
            {
                var dis = new StringBuilder();


                for (var i = 0; i < processed[rtidx[idx]].Count; i++)
                {
                    dis.Append("\t" + processed[rtidx[idx]][i]);
                }
                sw.WriteLine(rtidx[idx] + dis.ToString());
            }
            sw.Close();

            _msPlot.PlotHistogram(xvalue, "", "int", 1d);
        }


        private void featureMzPlotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _msPlot.ClearCurves();
            var detected = new List<PepFeature>();

            var maxRT = Parameter.RT_max_cutoff;
            var rt = Parameter.ElutingRTMin;
            rt = 5d;
            var minRT = Parameter.RT_min_cutoff;


            List<double> analyzedMZs = new List<double>();
            List<double> detectedMzs = new List<double>();

            foreach (PepFeature feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(minRT,maxRT))
            //foreach (var feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(minRT, maxRT, rt))
            {
                //if (feature.SummedIntensity < 100000)
                //    continue;
                if (feature.Charge == 1 || feature.Charge == 5)
                    continue;
                detected.Add(feature);
                detectedMzs.Add(feature.Mz);
            }
            Console.WriteLine("number of detected features:" + detected.Count.ToString());
            var interval = 10d;
            _msPlot.PlotHistogram(detected, "Detected features", interval);
            var analyzed = new List<PepFeature>();
            foreach (PepFeature feature in _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(minRT, maxRT))
            //foreach (var feature in _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures(minRT, maxRT, rt))
            {
                if (feature.Charge == 1 || feature.Charge == 5)
                    continue;
                analyzed.Add(feature);
                analyzedMZs.Add(feature.Mz);
            }
            _msPlot.PlotHistogram(analyzed, "Analyzed features", interval);

            var percentage = Math.Round((double) analyzed.Count/(detected.Count), 4)*100;

            //List<PepFeature> targeted = new List<PepFeature>();
            //foreach (PepFeature feature in _currentFeatureManager.TargetedFeatures())
            //    targeted.Add(feature);
            //_msPlot.PlotHistogram(targeted, "Targeted features");
            // finish this later

            string outputfile = _outputdir + @"/" + _projectManager.CurrentProject.ID + "_sampled_feature_over_mz.csv";
            Dictionary<string, List<double>> dataseries = new Dictionary<string, List<double>>();
            dataseries.Add("analyzed", analyzedMZs);
            dataseries.Add("detected", detectedMzs);
            Output.OuptputDataSeies(dataseries, outputfile);





            _msPlot.ComputePercentage("Detected features", "Analyzed features", "Analyzed / Detected Features",
                false, 0);
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            var filename = _outputdir + _projectManager.CurrentProject.ID + "_SampledFeature.png";
            _msPlot.SaveCurrentPic(filename);
        }


        private void sampledMzRegionPlotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _msPlot.ClearCurves();
            //List<IsolatedRegion> matchedMS2 = _currentFeatureManager.
            var totalMS2 = new List<double>();
            var nomatched = new List<double>();
            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
                totalMS2.Add(scan.TargetPrecursorMz);

            foreach (var scan in _projectManager.CurrentProject.NomatchedRegions())
            {
                nomatched.Add(scan.TargetPrecursorMz);
                totalMS2.Add(scan.TargetPrecursorMz);
            }
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
            _msPlot.PlotHistogram(totalMS2, "All MS2");
            _msPlot.PlotHistogram(nomatched, "NoMatched MS2");

            _msPlot.CurrentPane.XAxis.Scale.Min = 380;
            _msPlot.CurrentPane.XAxis.Scale.Max = 1420;

            _msPlot.ComputePercentage("All MS2", "NoMatched MS2", "NoMatched MS2/Total MS2", false, 0);
        }

        private void mS2AcrossRTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var totalMS2 = new List<double>();
            var nomatched = new List<double>();
            var ms2withSingle = new List<double>();
            var ms2withMultiple = new List<double>();

            foreach (var scan in _projectManager.CurrentProject.FeatureManager.IsolatedRegions())
            {
                totalMS2.Add(scan.RetentionTime);
                if (scan.CoIsolatedIons.Count ==1)
                    ms2withSingle.Add(scan.RetentionTime);
                else
                    ms2withMultiple.Add(scan.RetentionTime);
            }
            foreach (var scan in _projectManager.CurrentProject.NomatchedRegions())
            {
                nomatched.Add(scan.RetentionTime);
                totalMS2.Add(scan.RetentionTime);
            }

            Dictionary<string, List<double>> dataseries = new Dictionary<string, List<double>>();
            dataseries.Add("total MS2", totalMS2);
            dataseries.Add("MS2 with single p", ms2withSingle);
            dataseries.Add("MS2 with multiple p", ms2withMultiple);
            dataseries.Add("no matched", nomatched);
            string filepath = _outputdir + @"\" + _projectManager.CurrentProject.ID + "_MS2_scan_rt.csv";
            Output.OuptputDataSeies(dataseries,filepath);


            _msPlot.PlotHistogram(totalMS2, "All MS2", "RT", 1);
            _msPlot.PlotHistogram(nomatched, "NoMatched MS2", "RT", 1);
            var percentage = Math.Round(((double) (nomatched.Count))/(totalMS2.Count), 4)*100;
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            _msPlot.CurrentPane.XAxis.Scale.Min = 0;
            _msPlot.CurrentPane.XAxis.Scale.MaxAuto = true;
            _msPlot.ComputePercentage("All MS2", "NoMatched MS2", "NoMatched MS2/Total MS2", false, 0);
        }

        private void outputFeatureNumbersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter(_outputdir+@"\features");
            Dictionary<int,int> detected  = new Dictionary<int, int>();
            Dictionary<int,int> analyzed = new Dictionary<int, int>();
            Dictionary<int,int> id = new Dictionary<int, int>();
            sw.WriteLine("File\tNumber\tCharge\tType");
            for (int i = 1; i <= 5; i ++)
            {
                //sw.Write("\tCharge"+i+" (detect)\tCharge"+i+" (analyzed)\tCharge"+i+" (id)");
                detected.Add(i,0);
                analyzed.Add(i,0);
                id.Add(i,0);
            }
            //sw.WriteLine();

            
            foreach (var project in _projectManager.AllProjects)
            {
                foreach (var pfeature in project.FeatureManager.PeptideFeatures())
                {
                    detected[pfeature.Charge]++;
                    if (project.FeatureManager.IsAnalyzed(pfeature))
                        analyzed[pfeature.Charge]++;
                    if (project.FeatureManager.IsIdentified(pfeature))
                        id[pfeature.Charge]++;
                }
                //sw.Write(project.ID);
                for (int i = 1; i <= 5; i++)
                {
                    //sw.Write("\t"+detected[i]+"\t"+analyzed[i]+"\t"+id[i]);
                    sw.WriteLine(project.ID + "\t" + detected[i] + "\t" + i + "\tDetected");
                    sw.WriteLine(project.ID + "\t" + analyzed[i] + "\t" + i + "\tAnalyzed");
                    sw.WriteLine(project.ID + "\t" + id[i] + "\t" + i + "\tID");

                    detected[i] = 0;
                    analyzed[i] = 0;
                    id[i] = 0;
                }
                //sw.WriteLine();
            }
            sw.Close();
        }

        private void featureNumberAcrossRTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //_msPlot.ClearCurves();
            var detected = new List<PepFeature>();
            var maxRT = Parameter.RT_max_cutoff;
            var rt = Parameter.ElutingRTMin;
            //double rt = 0d;
            //maxRT = 70d;
            var percentage = 0d;
            List<double> detectedRTs = new List<double>();
            foreach (var feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                if (feature.Charge == 1 || feature.Charge==5)
                    continue;
                detected.Add(feature);
                detectedRTs.Add(feature.BestTime);
            }
            _msPlot.PlotHistogramAcrossRT(detected, "Detected features");


            var analyzed = new List<PepFeature>();
            List<double> analyzedRTs = new List<double>();
            foreach (var feature in _projectManager.CurrentProject.FeatureManager.AnalyzedFeatures())
            {
                if (feature.Charge == 1 || feature.Charge == 5)
                    continue;
                analyzed.Add(feature);
                analyzedRTs.Add(feature.BestTime);
            }
            percentage = Math.Round((double) analyzed.Count/(detected.Count), 4)*100;
            _msPlot.PlotHistogramAcrossRT(analyzed, "Analyzed features");

            Dictionary<string, List<double>> dataseries = new Dictionary<string, List<double>>();
            dataseries.Add("analyzed feature RT", analyzedRTs);
            dataseries.Add("detected feature RT", detectedRTs);
            string filepath = _outputdir + @"/" + _projectManager.CurrentProject.ID + "_feature_over_RT.csv";
            Output.OuptputDataSeies(dataseries, filepath);
            //List<PepFeature> targeted = new List<PepFeature>();
            //foreach (PepFeature feature in _currentFeatureManager.TargetedFeatures())
            //    targeted.Add(feature);
            //_msPlot.PlotHistogram(targeted, "Targeted features");
            // finish this later

            _msPlot.ComputePercentage("Detected features", "Analyzed features", "Analyzed/Detected (%)", false, 0);

            _msPlot.FigureTitle(_projectManager.CurrentProject.ID + " (" + percentage + "%)");
            var filename = _outputdir + _projectManager.CurrentProject.ID + "_SampledFeature_RT.png";
            _msPlot.SaveCurrentPic(filename);
        }

        private void featureRTDistributionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sampled = new List<PepFeature>();
            var unsampled = new List<PepFeature>();
            var maxRT = Parameter.RT_max_cutoff;
            var rt = Parameter.ElutingRTMin;
            //foreach (PepFeature feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures(0, maxRT,rt))
            foreach (var feature in _projectManager.CurrentProject.FeatureManager.PeptideFeatures())
            {
                if (_projectManager.CurrentProject.FeatureManager.FeatureToScans(feature).Count == 0)
                    unsampled.Add(feature);
                else
                    sampled.Add(feature);
            }
            _msPlot.PlotPeptideAverageRT(sampled, "Analyzed features");
            _msPlot.PlotPeptideAverageRT(unsampled, "NonSampled features");
            _msPlot.FigureTitle(_projectManager.CurrentProject.ID);
        }


        

        #endregion

        private void featureChargeDisOverMzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Project project = _projectManager.CurrentProject;
            Dictionary<int,List<PepFeature>> chargedPep = new Dictionary<int, List<PepFeature>>();

            foreach (var pep in  project.FeatureManager.PeptideFeatures())
            {
                
                if (!chargedPep.ContainsKey(pep.Charge))
                    chargedPep.Add(pep.Charge,new List<PepFeature>());
                chargedPep[pep.Charge].Add(pep);
            }

            foreach (int charge in chargedPep.Keys)
            {
                _msPlot.ClearCurves();
                List<PepFeature> peps = chargedPep[charge];
                List<double> detects = new List<double>();
                List<double> analyze = new List<double>();
                foreach (var pep in peps)
                {
                    detects.Add(pep.Mz);
                    if (project.FeatureManager.IsAnalyzed(pep))
                        analyze.Add(pep.Mz);
                }
                
                _msPlot.PlotHistogram(detects,"Detected","Count","MZ",10d,false);
                _msPlot.PlotHistogram(analyze, "Analyzed", "Count", "MZ", 10d, false);
                _msPlot.ComputePercentage("Detected","Analyzed","Precentage",false,20);
                double percentage = (analyze.Count/(double) detects.Count)*100;

                string title = project.ID + "_z" + charge+" ("+percentage+"%)";
                _msPlot.FigureTitle(title);
                string filename = _outputdir + @"\" + project.ID + "_charge" + charge + "_pepOverMz.png";
                _msPlot.SaveCurrentPic(filename);
            }
            
        }

        private void featureChargeDisOverTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Dictionary<string,List<Project>> groups = new Dictionary<string, List<Project>>();
            foreach (Project project in _projectManager.AllProjects)
            {
                string type = "";
                if (project.ID.Contains("_DDA_"))
                    type = "DDA";
                else if (project.ID.Contains("_DDAp_"))
                    type = "DDAp";
                else if (project.ID.Contains("_DIA_"))
                    type = "DIA";
                else
                    type = "Others";

                if (!groups.ContainsKey(type))
                    groups.Add(type,new List<Project>());
                groups[type].Add(project);
            }


            foreach (string g in groups.Keys)
            {
                foreach (Project project in groups[g])
                {

                    // get bined list for each project and calculate mean and sd
                    
                }
            }


            //Project project = _projectManager.CurrentProject;
            //Dictionary<int, List<PepFeature>> chargedPep = new Dictionary<int, List<PepFeature>>();

            //foreach (var pep in project.FeatureManager.PeptideFeatures())
            //{

            //    if (!chargedPep.ContainsKey(pep.Charge))
            //        chargedPep.Add(pep.Charge, new List<PepFeature>());
            //    chargedPep[pep.Charge].Add(pep);
            //}

            //foreach (int charge in chargedPep.Keys)
            //{
            //    _msPlot.ClearCurves();
            //    List<PepFeature> peps = chargedPep[charge];
            //    List<double> detects = new List<double>();
            //    List<double> analyze = new List<double>();
            //    foreach (var pep in peps)
            //    {
            //        detects.Add(pep.RT[0]);
            //        if (project.FeatureManager.IsAnalyzed(pep))
            //            analyze.Add(pep.RT[0]);
            //    }

            //    _msPlot.PlotHistogram(detects, "Detected", "Count", "RT", 1d, false);
            //    _msPlot.PlotHistogram(analyze, "Analyzed", "Count", "RT", 1d, false);
            //    _msPlot.ComputePercentage("Detected", "Analyzed", "Precentage", false, 20);
            //    double percentage = (analyze.Count / (double)detects.Count) * 100;

            //    string title = project.ID + "_Z"+charge+"rt (" + percentage + "%)";
            //    _msPlot.FigureTitle(title);
            //    string filename = _outputdir + @"\" + project.ID +"z_"+ charge+"_pepOverRT.png";
            //    _msPlot.SaveCurrentPic(filename);
            //}


        }



        private void outputFeatureDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //string filename = _outputdir + @"\featureDetails.csv";
            //StreamWriter sw = new StreamWriter(filename);

            //sw.WriteLine("Filename\tPepNo\tCharge\tIntensity\tSumIntensity\tMZ\tRT1\tRT2\tAnalyzed\tID\tNoMS2");
            //int maxMS2 = 0;
            //foreach (Project project in _projectManager.AllProjects)
            //{
            //    int no = 0;

            //    foreach (PepFeature pep in project.FeatureManager.PeptideFeatures())
            //    {
            //        no++;
            //        int noMS2 = project.FeatureManager.FeatureToScans(pep).Count;
            //        sw.WriteLine(project.ID + "\t" + no + "\t" + pep.Charge + "\t" + pep.Intensity + "\t" +
            //                     pep.SummedIntensity + "\t" + pep.Mz + "\t" + pep.RT[0] + "\t" + pep.RT[1] + "\t" +
            //                     project.FeatureManager.IsAnalyzed(pep) + "\t" +
            //                     project.FeatureManager.IsIdentified(pep) + "\t" + noMS2);
            //        if (noMS2 > maxMS2)
            //            maxMS2 = noMS2;
            //    }
            //    Console.WriteLine(project.ID + "\t" + maxMS2);
            //}

            //sw.Close();



            /////// number of MS2 per feature
            //Dictionary<int, Dictionary<string, int>> countingRecord = new Dictionary<int, Dictionary<string, int>>();
            //List<string> order = new List<string>();
            //for (int i = 0; i <= maxMS2; i ++)
            //{
            //    countingRecord.Add(i, new Dictionary<string, int>());
            //    foreach (Project project in _projectManager.AllProjects)
            //    {
            //        countingRecord[i].Add(project.ID, 0);
            //        if (i == 1)
            //            order.Add(project.ID);
            //    }
            //}

            //foreach (Project project in _projectManager.AllProjects)
            //{
            //    foreach (PepFeature pep in project.FeatureManager.PeptideFeatures())
            //    {
            //        int noMS2 = project.FeatureManager.FeatureToScans(pep).Count;
            //        countingRecord[noMS2][project.ID]++;
            //    }
            //}




            //filename = _outputdir + @"\NoMS2PerFeature";
            //sw = new StreamWriter(filename);
            //sw.Write("#MS2 per feature");
            //for (int i = 0; i < order.Count; i++)
            //    sw.Write("\t"+order[i]);
            //sw.WriteLine();
            //foreach (int count in countingRecord.Keys)
            //{
            //    sw.Write(count);
            //    for (int idx =0 ; idx < order.Count; idx++)
            //    {
            //        sw.Write("\t"+countingRecord[count][order[idx]]);
            //    }
            //    sw.WriteLine();
            //}
            //sw.Close();


            string filename = _outputdir + @"\SampledFeatureRT";
            StreamWriter sw = new StreamWriter(filename);
            List<string> order = new List<string>();
            Dictionary<double,Dictionary<string,int>> rtRecord = new Dictionary<double, Dictionary<string, int>>();
            sw.Write("RT");
            foreach (Project project in _projectManager.AllProjects)
            {
                order.Add(project.ID);
                sw.Write("\t"+project.ID);
                foreach (PepFeature pep in project.FeatureManager.PeptideFeatures())
                {
                    if (!project.FeatureManager.IsAnalyzed(pep))
                        continue;
                    double rt = (pep.RT[1] - pep.RT[0])*60;
                    if (!rtRecord.ContainsKey(rt))
                    {
                        rtRecord.Add(rt, new Dictionary<string, int>());
                        foreach (Project pro in _projectManager.AllProjects)
                            rtRecord[rt].Add(pro.ID,0);
                    }
                    rtRecord[rt][project.ID]++;
                }
            }
            sw.WriteLine();


            foreach (double rt in rtRecord.Keys)
            {
                sw.Write(rt);
                for (int idx = 0; idx < order.Count; idx++)
                    sw.Write("\t"+rtRecord[rt][order[idx]]);
                sw.WriteLine();
            }
            sw.Close();
            

        }


        
    }
}