﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileReader;

namespace MSFeature
{
    internal class FileManager
    {
        public readonly string Fileprefix;

        public FileManager(string fileprefix)
        {
            Fileprefix = fileprefix;
        }

        private Hardklor _hardklor;
        private Kronik _kronik;
        private RawDataInfo _rawfile;
        private MS2TitleInfo _ms2;
        private MS2TitleInfo _nomatched;
        private Percolator _search;

        public Hardklor Hardklor {get { return _hardklor; }}
        public Kronik Kronik { get { return _kronik; }}
        public Percolator Percolator {get { return _search; }}
        public RawDataInfo Raw {get { return _rawfile; }}
        public MS2TitleInfo MS2 {get { return _ms2; }}
        public MS2TitleInfo NoMatched {get { return _nomatched; }}


            public void AddKronikFile(string filepath)
            {
                if (Path.GetFileNameWithoutExtension(filepath) == Fileprefix)
            {
                _kronik = new Kronik(filepath, Path.GetFileNameWithoutExtension(filepath));
            }
        }

        public void AddHardklorFile(string filepath)
        {
            if (Path.GetFileNameWithoutExtension(filepath) == Fileprefix)
            {
                _hardklor = new Hardklor(filepath, Path.GetFileNameWithoutExtension(filepath));
            }
        }

        public void AddRawfile(string filepath)
        {
            if (Path.GetFileNameWithoutExtension(filepath) == Fileprefix)
            {
                _rawfile = FileReader.RawReader.ReadRawfileHeader(filepath);
            }
        }

        public void AddSearchresult(string filepath)
        {
            string fileprefix = Path.GetFileNameWithoutExtension(filepath);
            fileprefix = fileprefix.Substring(0, fileprefix.IndexOf('.'));
            if (fileprefix  == Fileprefix)
            {
                _search = new Percolator(filepath, fileprefix);
            }
        }

        public void AddMS2file(string filepath)
        {
            string fileprefix = Path.GetFileNameWithoutExtension(filepath);
            fileprefix = fileprefix.Substring(0, fileprefix.IndexOf('.'));
            if (fileprefix == Fileprefix)
            {
                _ms2 = new MS2TitleInfo(filepath, fileprefix);
            }
        }

        public void AddNonMatchedMS2(string filepath)
        {
            string fileprefix = Path.GetFileNameWithoutExtension(filepath);
            fileprefix = fileprefix.Substring(0, fileprefix.IndexOf('.'));
            if (fileprefix == Fileprefix)
            {
                _nomatched = new MS2TitleInfo(filepath, fileprefix);
            }
        }
    }
}
