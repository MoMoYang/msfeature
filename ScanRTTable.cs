﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileReader;

namespace MSFeature
{
    public class ScanRTTable
    {
        public readonly  List<IsolatedRegion> ScanList = new List<IsolatedRegion>();

        public ScanRTTable(List<IsolatedRegion> scans)
        {
            ScanList = scans;
        }
    }
}
