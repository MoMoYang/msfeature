﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using FileReader;
using ZedGraph;

namespace MSFeature
{
    public partial class MSPlot : UserControl
    {
        public MSPlot()
        {
            InitializeComponent();
            Colorlist = ColorCollection.Colorlist;
        }

        private List<Color> Colorlist = null;

        public GraphPane CurrentPane
        {
            get
            {
                return zedGraphControl1.GraphPane;
            }
        }


        public void twoDPlot(List<PepFeature> features ,string title,Color baseColor, Color monoColor)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.Title.Text = title;
            myPane.XAxis.Title.Text = "RT";
            myPane.YAxis.Title.Text = "m/z";
            foreach (PepFeature pep in features)
            {
                PointPairList basePoint = new PointPairList();
                //if (pep.BaseMz > maxMZ || pep.BaseMz < minMZ)
                //    continue;
                basePoint.Add(pep.RT[0],pep.BaseMz);
                basePoint.Add(pep.RT[1],pep.BaseMz);
                LineItem myCurve = myPane.AddCurve("", basePoint, baseColor, SymbolType.None);
                myCurve.Line.Width = 2.0f;

                PointPairList monoPoint = new PointPairList();
                monoPoint.Add(pep.RT[0], pep.Mz);
                monoPoint.Add(pep.RT[1], pep.Mz);
                myCurve = myPane.AddCurve("", monoPoint, monoColor, SymbolType.None);
                myCurve.Line.Width = 2.0f;

            }

            myPane.XAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            //myPane.YAxis.Scale.Min = 1200;
            //myPane.YAxis.Scale.Max = 1260;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void plotAnalyzedRegion(List<IsolatedRegion> scans,string title, Color ms2Color)
        {

            var myPane = zedGraphControl1.GraphPane;
            myPane.Title.Text = title;
            myPane.XAxis.Title.Text = "RT";
            myPane.YAxis.Title.Text = "m/z";

            foreach (IsolatedRegion scan in scans)
            {
                PointPairList point  = new PointPairList();
                double lowerMZ = scan.TargetPrecursorMz - 1;
                double higherMZ = scan.TargetPrecursorMz + 1;
                //if (lowerMZ > maxMZ || higherMZ < minMZ)
                //    continue;
                point.Add(scan.RetentionTime,lowerMZ);
                point.Add(scan.RetentionTime, higherMZ);
                LineItem myCurve = myPane.AddCurve("", point, ms2Color, SymbolType.None);
                myCurve.Line.Width = 1.0f;
            }

            myPane.XAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            //myPane.YAxis.Scale.Min = minMZ - 50;
            //myPane.YAxis.Scale.Max = maxMZ + 50;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
            
        }

        public void PlotFeatureRTAcrossMZ(List<PepFeature> features)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "MZ";
            myPane.YAxis.Title.Text = "Feature width (sec)";

            Dictionary<double, List<double>> rtCrossMz = new Dictionary<double, List<double>>();

            List<double> mzs = new List<double>();
            foreach (PepFeature pep in features)
            {
                double mz = Math.Round(pep.BaseMz / 10d, 0) * 10;
                double rt = (pep.RT[1] - pep.RT[0])*60;

                if (!rtCrossMz.ContainsKey(mz))
                {
                    rtCrossMz.Add(mz,new List<double>());
                    mzs.Add(mz);
                }
                rtCrossMz[mz].Add(rt);
            }
            mzs.Sort();

            PointPairList averageRT = new PointPairList();
            PointPairList maxRT = new PointPairList();
            PointPairList minRT = new PointPairList();

            foreach (double mz in mzs)
            {
                double max = double.MinValue;
                double min = double.MaxValue;
                double average = 0d;
                foreach (double rt in rtCrossMz[mz])
                {
                    average += rt;
                    if (max < rt)
                        max = rt;
                    if (min > rt)
                        min = rt;
                }
                average = average/rtCrossMz[mz].Count;
                averageRT.Add(mz,average);
                maxRT.Add(mz,max);
                minRT.Add(mz,min);
            }

            int idx = myPane.CurveList.Count;
            LineItem avgCurve = myPane.AddCurve("Average RT", averageRT, Colorlist[idx]);
            LineItem minCurve = myPane.AddCurve("Min RT", minRT, Colorlist[idx + 1]);
            LineItem maxCurve = myPane.AddCurve("Max RT", maxRT, Colorlist[idx + 2]);
            
            foreach (LineItem myCurve in myPane.CurveList)
            {
                //myCurve.IsY2Axis = true;
                myCurve.Symbol.Size = 3;
                myCurve.Line.IsVisible = true;
                myCurve.Line.Width = 2.5f;
                myCurve.Symbol.IsVisible = true;
                myCurve.Symbol.Fill = new Fill(myCurve.Line.Color);
            }
            maxCurve.IsY2Axis = true;

            //myPane.XAxis.Scale.MinAuto = true;
            //myPane.XAxis.Scale.MaxAuto = true;
            myPane.XAxis.Scale.Max = 1420;
            myPane.XAxis.Scale.Min = 380;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;

            myPane.Y2Axis.IsVisible = true;
            myPane.Y2Axis.Scale.MinAuto = true;
            myPane.Y2Axis.Scale.MaxAuto = true;

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotPeptideAverageRT(List<PepFeature> features, string curvelabel)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "Elution time (Sec)";
            myPane.YAxis.Title.Text = "Feature Count";
            Dictionary<double, int> rtCount = new Dictionary<double, int>();
            List<double> mzs = new List<double>();
            double maxTime = 0d;
            double interval = 0.1;
            double factor = 1/interval;
            foreach (PepFeature pep in features)
            {
                double rt = pep.RT[1] - pep.RT[0];

                //int min = (int)rt;
                //double sec = (rt - min) * 60;
                //sec = sec + min * 60;
                //double sec = rt*60;
                //double time = Math.Round(sec * factor, 0)*interval;
                double time = rt;
                if (!rtCount.ContainsKey(time))
                {
                    rtCount.Add(time, 0);
                    mzs.Add(time);
                    if (maxTime < time)
                        maxTime = time;
                }

                rtCount[time]++;
            }
            mzs.Sort();

            PointPairList list = new PointPairList();
            foreach (double mz in mzs)
            {
                list.Add(mz, (double)rtCount[mz]);
            }
            int idx = myPane.CurveList.Count + 1;
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx - 1]);
            myBar.Bar.Fill = new Fill(Colorlist[idx - 1]);

            //myPane.XAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.Max = maxTime + 0.1;
            myPane.XAxis.Scale.Min = 0d;
            //myPane.XAxis.Scale.Max = 1420d;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotHistogramAcrossRT(List<PepFeature> features, string curvelabel)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "RT (min)";
            myPane.YAxis.Title.Text = "Feature Count";
            Dictionary<double, int> mzCount = new Dictionary<double, int>();
            List<double> mzs = new List<double>();
            foreach (PepFeature pep in features)
            {
                double mz = Math.Round(pep.BestTime, 0);

                if (!mzCount.ContainsKey(mz))
                {
                    mzCount.Add(mz, 0);
                    mzs.Add(mz);
                }
                mzCount[mz]++;
            }
            mzs.Sort();

            PointPairList list = new PointPairList();
            foreach (double mz in mzs)
            {
                list.Add(mz, (double)mzCount[mz]);
            }
            int idx = myPane.CurveList.Count + 1;
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx - 1]);
            myBar.Bar.Fill = new Fill(Colorlist[idx - 1]);
            myPane.BarSettings.Type = BarType.Cluster;
            //myPane.XAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.XAxis.Scale.Min = 0d;
            //myPane.XAxis.Scale.Max = 1400d;
            myPane.YAxis.Scale.MinAuto = true;
            //myPane.YAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.Max = 1400d;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotFeatureAnalyzeTimes(List<int> count, string curvelabel, bool normalize)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "Number of MS2 scans";
            if (normalize)
                myPane.YAxis.Title.Text = "Percentage of features(%)";
            else 
                myPane.YAxis.Title.Text = "Feature Count";
            myPane.Title.Text = "";

            Dictionary<int,int> countCount = new Dictionary<int, int>();

            foreach (int time in count)
            {
                if (!countCount.ContainsKey(time))
                    countCount.Add(time,0);
                countCount[time]++;
            }
            List<int> ms2count = countCount.Keys.ToList();
            ms2count.Sort();
            
            PointPairList list = new PointPairList();
            string[] labels = new string[ms2count.Count];
            for (int i = 0; i < ms2count.Count; i ++)
            {
                if (normalize)
                {
                    //double percentage = Math.Round((double)(countCount[ms2count[i]]) / count.Count,4)*100;
                    double percentage = ((int)(countCount[ms2count[i]]*10000 / count.Count))/100d;
                    
                    list.Add(ms2count[i], percentage);
                }
                else
                    list.Add(ms2count[i], countCount[ms2count[i]]);
                labels[i] = ms2count[i].ToString();
            }


            int idx = myPane.CurveList.Count;
            
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx]);
            myBar.Bar.Fill = new Fill(Colorlist[idx]);
            myPane.XAxis.Scale.Max = ms2count[ms2count.Count - 1]+1;
            myPane.XAxis.Scale.Min = 0;
            myPane.XAxis.Type = AxisType.Text;
            myPane.XAxis.Scale.TextLabels = labels;
            myPane.YAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();

        }

        public void Plot(List<double> xvalue, List<double> yvalue, string curvelable, string xlabel, string ylabel)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = xlabel;
            myPane.YAxis.Title.Text = ylabel;
            int cidx = myPane.CurveList.Count;

            BarItem bar = myPane.AddBar(curvelable, xvalue.ToArray(), yvalue.ToArray(), Colorlist[cidx]);
            bar.Bar.Fill = new Fill(Colorlist[cidx]);
            myPane.BarSettings.Type = BarType.Cluster;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            myPane.XAxis.Scale.Min = 380d;
            myPane.XAxis.Scale.Max = 1420d;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotHistogram(List<PepFeature> features ,string curvelabel, double interval)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "MZ";
            myPane.YAxis.Title.Text = "Feature Count";
            Dictionary<double,int> mzCount = new Dictionary<double, int>();
            List<double> mzs = new List<double>();
            foreach (PepFeature pep in features)
            {
                //double mz = Math.Round(pep.BaseMz / interval, 0) * interval;
                //double mz = (int) Math.Round(pep.BaseMz/interval)*interval;
                double mz = ConverToBinIdx(pep.BaseMz, interval);
                if (!mzCount.ContainsKey(mz))
                {
                    mzCount.Add(mz, 0);
                    mzs.Add(mz);
                }
                mzCount[mz]++;
            }
            mzs.Sort();
            
            PointPairList list = new PointPairList();
            foreach (double mz in mzs)
            {
                list.Add(mz, (double)mzCount[mz]);
            }
            int idx = myPane.CurveList.Count+1;
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx - 1]);
            myBar.Bar.Fill = new Fill(Colorlist[idx-1]);
            myPane.BarSettings.Type = BarType.Cluster;
            
            //myPane.XAxis.Scale.MinAuto = true;
            //myPane.XAxis.Scale.MaxAuto = true;
            myPane.XAxis.Scale.Min = 380d;
            myPane.XAxis.Scale.Max = 1420d;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotHistogramCountByUniqueValue(Dictionary<double,List<string>> values, string curvelabel,string xlabel,string ylabel, double factor)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = xlabel;
            myPane.YAxis.Title.Text = ylabel;

            Dictionary<double,Dictionary<string,int>> Counts = new Dictionary<double, Dictionary<string, int>>();
            List<double> mzs = new List<double>();
            double total = 0d;


            foreach (double value in values.Keys)
            {
                double mz = Math.Round(value / factor, 0) * factor;
                if (!Counts.ContainsKey(mz))
                {
                    Counts.Add(mz, new Dictionary<string, int>());
                    mzs.Add(mz);
                }
                foreach (string id in values[value])
                {
                    if (!Counts[mz].ContainsKey(id))
                        Counts[mz].Add(id,0);
                    Counts[mz][id]++;
                }
            }


            mzs.Sort();
            PointPairList list = new PointPairList();
            foreach (double mz in mzs)
            {
                list.Add(mz, (double)Counts[mz].Keys.Count);
            }
            int idx = myPane.CurveList.Count + 1;
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx - 1]);
            myBar.Bar.Fill = new Fill(Colorlist[idx - 1]);

            myPane.XAxis.Scale.MinAuto = true;
            myPane.BarSettings.Type = BarType.Cluster;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();

        }




        public void PlotHistogram(List<double> values, string curvelable)
        {
            PlotHistogram(values, curvelable, "MZ", 10d);
        }

        public void PlotHistogram(List<double> xValues, string curvelabel, string xlabel, double interval)
        {
            PlotHistogram(xValues, curvelabel,"Count" ,xlabel, interval, false);
        }
        

        public void PlotHistogram(List<double> xValues, string curvelabel, string ylabel, string xlabel,double factor,bool normalizeToSelf)
        {

            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = xlabel;
            myPane.YAxis.Title.Text = ylabel;
            Dictionary<double, int> mzCount = new Dictionary<double, int>();
            List<double> mzs = new List<double>();
            double total = 0d;
            foreach (double pep in xValues)
            {
                //double mz = Math.Round(pep / interval, 0) * interval;
                double mz = ConverToBinIdx(pep, factor);

                if (!mzCount.ContainsKey(mz))
                {
                    mzCount.Add(mz, 0);
                    mzs.Add(mz);
                }
                mzCount[mz]++;
                total++;
            }
            mzs.Sort();

            PointPairList list = new PointPairList();
            foreach (double mz in mzs)
            {
                if (normalizeToSelf)
                {
                    double perc = Math.Round(mzCount[mz]/total, 4)*100;
                    list.Add(mz,perc );
                }
                else
                    list.Add(mz, (double) mzCount[mz]);
            }
            int idx = myPane.CurveList.Count + 1;
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx - 1]);
            myBar.Bar.Fill = new Fill(Colorlist[idx - 1]);

            myPane.XAxis.Scale.MinAuto = true;
            //myPane.XAxis.Scale.Max = 1420d;
            //myPane.YAxis.Scale.MinAuto = true;
            //myPane.YAxis.Scale.Max = 250d;
            myPane.BarSettings.Type = BarType.Cluster;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }




        public void PlotAppendHistogram(List<double> xValues, string curvelabel, string xlabel, string ylabel, double factor)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = xlabel;
            myPane.YAxis.Title.Text = ylabel;
            Dictionary<double, int> mzCount = new Dictionary<double, int>();
            List<double> mzs = new List<double>();
            double total = 0;
            foreach (double value in xValues)
            {
                //double mz = Math.Round(value / interval, 0) * interval;
                double mz = ConverToBinIdx(value, factor);

                if (!mzCount.ContainsKey(mz))
                {
                    mzCount.Add(mz, 0);
                    mzs.Add(mz);
                }
                mzCount[mz]++;
                total++;
            }
            mzs.Sort();

            PointPairList list = new PointPairList();
            foreach (double mz in mzs)
            {
                //if (!normalizeToTotal)
                    list.Add(mz, (double)mzCount[mz]);
                //else
                //{
                //    double per = Math.Round(mzCount[mz] / total, 4) * 100;
                //    list.Add(mz, per);
                //}

            }
            int idx = myPane.CurveList.Count + 1;
            BarItem myBar = myPane.AddBar(curvelabel, list, Colorlist[idx - 1]);
            myBar.Tag = idx + 1;
            myBar.Bar.Fill = new Fill(Colorlist[idx - 1]);

            //myPane.XAxis.Scale.Min = 0d;
            myPane.BarSettings.Type = BarType.Stack;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.XAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotBinnedDistribution(List<double> peptide, string label)
        {
            double interval = 10d;


        }

        public void ComputePercentage(string detlabel, string analabel, string curvelabel, bool sum, int minCountValue)
        {
            var myPane = zedGraphControl1.GraphPane;
            CurveItem analyzed = myPane.CurveList[analabel];
            CurveItem detected = myPane.CurveList[detlabel];
            //CurveItem detected = myPane.CurveList[detlabel];
            Dictionary<double, double> anaCount = new Dictionary<double, double>();
            Dictionary<double, double> detCount = new Dictionary<double, double>();
            Dictionary<double, int> confirmkeys = new Dictionary<double, int>();

            //IPointList points = analyzed.Points;
            PointPairList points = (PointPairList)analyzed.Points;

            double totalDetected = 0d;
            double totalAnalyzed = 0d;
            foreach (PointPair point in points)
            {
                if (!anaCount.ContainsKey(point.X))
                    anaCount.Add(point.X, 0);
                if (!confirmkeys.ContainsKey(point.X))
                    confirmkeys.Add(point.X, 0);

                anaCount[point.X] += point.Y;
                totalAnalyzed += point.Y;

            }
            points = (PointPairList)detected.Points;

            foreach (PointPair point in points)
            {
                if (!detCount.ContainsKey(point.X))
                    detCount.Add(point.X, 0);
                if (!confirmkeys.ContainsKey(point.X))
                    confirmkeys.Add(point.X, 0);
                detCount[point.X] += point.Y;
                totalDetected += point.Y;
            }



            PointPairList percentages = new PointPairList();
            List<double> mzorder = confirmkeys.Keys.ToList();
            mzorder.Sort();
            foreach (double mz in mzorder)
            {
                double percent = 0d;
                if (anaCount.ContainsKey(mz))
                {
                    if (sum)
                    {
                        double total = anaCount[mz];
                        if (detCount.ContainsKey(mz))
                            total += detCount[mz];
                        if (total < minCountValue)
                            continue;

                        percent = (Math.Round((anaCount[mz] / total), 4)) * 100;
                    }
                    else
                        percent = (Math.Round((anaCount[mz] / detCount[mz]), 4)) * 100;
                }
                else
                    percent = 0;


                percentages.Add(mz, percent);
            }
            int idx = myPane.CurveList.Count;
            LineItem myCurve = myPane.AddCurve(curvelabel, percentages, Colorlist[idx]);
            myCurve.Tag = 0;
            //"Analyzed / Total Features"
            myCurve.IsY2Axis = true;
            myCurve.Symbol.Size = 3;
            myCurve.Line.IsVisible = true;
            myCurve.Line.Width = 2.5f;
            myCurve.Symbol.IsVisible = true;
            myCurve.Symbol.Fill = new Fill(Colorlist[idx]);

            myPane.YAxis.Scale.MinAuto = true;
            //myPane.YAxis.Scale.Max = 10d;
            myPane.YAxis.Scale.MaxAuto = true;
            myPane.Y2Axis.IsVisible = true;
            myPane.Y2Axis.Scale.Min = 0;
            myPane.Y2Axis.Scale.Max = 100;
            //myPane.Y2Axis.Scale.FontSpec.FontColor = Colorlist[idx];
            //myPane.Y2Axis.Title.FontSpec.FontColor = Colorlist[idx];
            myPane.Y2Axis.Scale.Align = AlignP.Inside;
            double coverage = Math.Round((totalAnalyzed / totalDetected), 4) * 100;
            Console.WriteLine("Coverage:" + coverage.ToString());

            myPane.CurveList.Sort(new CurveSort());
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotScotterPlot(List<double> xvalues,List<double> yvalues, string xlabel, string ylabel, double xfactor, double yfactor)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = xlabel;
            myPane.YAxis.Title.Text = ylabel;

            PointPairList list = new PointPairList();
            for (int idx =0 ; idx < xvalues.Count; idx++)
            {
                //double it = Math.Round(xvalues[idx]/xfactor, 0)*xfactor;
                //double ion = Math.Round(yvalues[idx]/yfactor, 0)*yfactor;
                double it = ConverToBinIdx(xvalues[idx], xfactor);
                double ion = ConverToBinIdx(yvalues[idx], yfactor);
                list.Add(it,ion);
            }


            int cidx = myPane.CurveList.Count;
            LineItem myCurve = myPane.AddCurve("", list, Colorlist[cidx], SymbolType.Circle);
            myCurve.Symbol.Fill = new Fill(Colorlist[cidx]);
            myCurve.Symbol.Size = 2f;
            myCurve.Line.IsVisible = false;
               
            myPane.XAxis.Scale.Min = 0d;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotLine(List<double> xvalue, List<double> yvalues, string curvelabel, double factor)
        {
            var myPane = zedGraphControl1.GraphPane;
            PointPairList list = new PointPairList();
            for (int idx = 0; idx < xvalue.Count; idx++)
            {
                //double it = Math.Round(xvalue[idx] / interval, 0) * interval;
                //double ion = Math.Round(yvalues[idx] / interval, 0) * interval;
                double it = ConverToBinIdx(xvalue[idx], factor);
                double ion = ConverToBinIdx(yvalues[idx], factor);

                list.Add(it, ion);
            }

            int cidx = myPane.CurveList.Count;
            LineItem myCurve = myPane.AddCurve(curvelabel, list, Colorlist[cidx], SymbolType.Circle);
            myCurve.Symbol.Fill = new Fill(Colorlist[cidx]);
            myCurve.Symbol.Size = 1f;
            myCurve.Line.IsVisible = true;

            myPane.XAxis.Scale.Min = 0d;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

   

        public void PlotBar(List<double> xvalue, List<double> yvalues, string curvelabel, double factor)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "Eluting time cut-off (Sec)";
            myPane.YAxis.Title.Text = "# Feature";

            PointPairList list = new PointPairList();
            for (int idx = 0; idx < xvalue.Count; idx++)
            {
                //double it = Math.Round(xvalue[idx] / interval, 0) * interval;
                //double ion = Math.Round(yvalues[idx] / interval, 0) * interval;
                double it = ConverToBinIdx(xvalue[idx], factor);
                double ion = ConverToBinIdx(yvalues[idx], factor);

                list.Add(it, ion);
            }

            int cidx = myPane.CurveList.Count;
            BarItem myCurve = myPane.AddBar(curvelabel, list, Colorlist[cidx]);
            //AddCurve(curvelabel, list, Colorlist[cidx], SymbolType.Circle);
            myCurve.Bar.Fill = new Fill(Colorlist[cidx]);

            myPane.XAxis.Scale.Min = 0d;
            
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void PlotPieChart(string label, double value)
        {
            var myPane = zedGraphControl1.GraphPane;
            int idx = myPane.CurveList.Count;
            PieItem slice = myPane.AddPieSlice(value, Colorlist[idx], 0.1d, label);

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        //"Detected features"

        public void PlotCurmulateCurve(string curvelabel,List<double> values, double interval)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "Peak width";
            myPane.YAxis.Title.Text = "Percentage (%)";
            values.Sort();
            List<int> counts = new List<int>();
            
            double max = values[values.Count - 1];
            double min = values[0] - interval;
            if (min < 0)
                min = 0;

            List<double> xvalues = new List<double>();
            for (double i = min; i <= (max + interval); i+= interval)
            {
                double  v = Math.Round(i, 2);
                xvalues.Add(v);
            }


            
            int lastone = 0;
            int count = 0;

            List<int> yvalues = new List<int>();
            for (int i = 0; i < xvalues.Count; i++)
            {
                count = 0;
                for (int idx = lastone; idx < values.Count; idx++)
                {
                    if (values[idx] < xvalues[i])
                        count ++;
                    else
                    {
                        yvalues.Add(count);
                        lastone = idx;
                        break;
                    }
                }
            }
            yvalues.Add(count);
            int check = 0;

            foreach (int c in yvalues)
                check += c;

            PointPairList list = new PointPairList();
            double sum = 0d;
            List<double> percentages = new List<double>();

            for (int i = 0; i < xvalues.Count; i++)
            {
                sum += yvalues[i];
                double percentage = Math.Round(sum/values.Count,4)*100;
                //list.Add(xvalues[i],percentage);
                percentages.Add(percentage);
            }

            int cidx = myPane.CurveList.Count;
            //LineItem myCurve = myPane.AddCurve(curvelabel, list, Colorlist[cidx]);
            LineItem myCurve = myPane.AddCurve(curvelabel, xvalues.ToArray(), percentages.ToArray(), Colorlist[cidx]);
            myCurve.Symbol.Type = SymbolType.Circle;
            myCurve.Symbol.Fill = new Fill(Colorlist[cidx]);
            myCurve.Symbol.Size = 1;

            //BarItem myCurve = myPane.AddBar(curvelabel, list, Colorlist[cidx]);
            ////AddCurve(curvelabel, list, Colorlist[cidx], SymbolType.Circle);
            //myCurve.Bar.Fill = new Fill(Colorlist[cidx]);

            //myPane.XAxis.Scale.Min = 0d;

            //myPane.XAxis.Scale.MaxAuto = true;
            //myPane.YAxis.Scale.MinAuto = true;
            //myPane.YAxis.Scale.MaxAuto = true;

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();

            
        }


        public void DoNormalizeOnAllCurve()
        {

            var myPane = zedGraphControl1.GraphPane;
            //Dictionary<double,int> confirmkeys = new Dictionary<double, int>();
            double totalDetected = 0d;

            Dictionary<string, Dictionary<double, double>> curveData = new Dictionary<string, Dictionary<double, double>>();
            List<string> curveNames = new List<string>();
            foreach (CurveItem curve in myPane.CurveList)
            {
                PointPairList points = (PointPairList)curve.Points;
                curveData.Add(curve.Label.Text,new Dictionary<double, double>());
                curveNames.Add(curve.Label.Text);
                foreach (PointPair point in points)
                {
                    curveData[curve.Label.Text].Add(point.X,point.Y);
                    //if (!confirmkeys.ContainsKey(point.X))
                    //    confirmkeys.Add(point.X, 0);
                    totalDetected += point.Y;
                }
            }

            myPane.CurveList.Clear();
            foreach (string curvelabel in curveNames)
            {
                PointPairList normalizedCurve = new PointPairList();
                Dictionary<double, double> data = curveData[curvelabel];
                foreach (double xvalue in data.Keys)
                {
                    double proportion = Math.Round(data[xvalue]/totalDetected, 4)*100;
                    normalizedCurve.Add(xvalue,proportion);
                }
                int idx = myPane.CurveList.Count;
                BarItem myBar = myPane.AddBar(curvelabel, normalizedCurve, Colorlist[idx]);
                myBar.Bar.Fill = new Fill(Colorlist[idx]);
                myBar.Tag = myPane.CurveList.Count;
            }

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void ClearCurves()
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.GraphObjList.Clear();
            myPane.CurveList.Clear();
            myPane.Chart.IsRectAuto = true;
            myPane.XAxis.Type = new AxisType();
            myPane.XAxis.Scale.MinAuto = true;
            myPane.XAxis.Scale.MaxAuto = true;
            myPane.YAxis.Scale.MinAuto = true;
            myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void ChartXAxisRange(double min, double max)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Scale.Max = max;
            myPane.XAxis.Scale.Min = min;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }
        public void ChartYAxisRange(double min, double max)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.YAxis.Scale.Max = max;
            myPane.YAxis.Scale.Min = min;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void FigureLegand(string content)
        {
            var myPane = zedGraphControl1.GraphPane;
            TextObj text = new TextObj(content,0.18F, 0.25F, CoordType.PaneFraction);
            text.Location.AlignH = AlignH.Center;
            text.Location.AlignV = AlignV.Bottom;
            text.FontSpec.Border.IsVisible = false;
            text.FontSpec.StringAlignment = StringAlignment.Center;
            myPane.GraphObjList.Add(text);

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void FigureTitle (string title)
        {
            var myPane = zedGraphControl1.GraphPane;
            myPane.Title.Text = title;

            
            //myPane.YAxis.Scale.MinAuto = true;
            //myPane.YAxis.Scale.MaxAuto = true;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public void SaveCurrentPic(string filename)
        {
            zedGraphControl1.GraphPane.GetImage().Save(filename,ImageFormat.Png);
        }

        private double ConverToBinIdx(double value, double interval)
        {
            double binIdx = 0d;
            if (interval > 1)
            {
                binIdx = (int) (value/interval)*interval;
            }
            else
            {
                int factor = (int)(1/interval);
                //binIdx = (value/factor)*interval;
                //if (value < 1)
                //    Console.Write("");
                binIdx = Math.Floor(value *factor)*interval;

            }
            return binIdx;
        }


    }



    public class CurveSort : IComparer<CurveItem>
    {
        public int Compare(CurveItem s1, CurveItem s2)
        {
            if (s1.Tag == null || s2.Tag == null)
                return 0;
            float c1 = float.Parse(s1.Tag.ToString());
            float c2 = float.Parse(s2.Tag.ToString());
            if (c1 > c2)
                return 1;
            else if (c1 < c2)
                return -1;
            else
                return 0;
        }
    }
}
