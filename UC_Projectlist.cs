﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MSFeature
{

    public delegate void SelectedProjectHandler(string projectName);
    public partial class UC_Projectlist : UserControl
    {
        public event SelectedProjectHandler SelectedProjectChanged;

        public UC_Projectlist()
        {
            InitializeComponent();
        }

        public void UpdateProjectList(object sender)
        {
            listBox_project.Items.Clear();
            foreach (Project project in (IEnumerable<Project>)sender)
            {
                listBox_project.Items.Add(project.ID);
            }
        }

        private void listBox_project_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox_project.SelectedIndex < 0)
                return;
            string name = listBox_project.Items[listBox_project.SelectedIndex].ToString();
            if (SelectedProjectChanged != null)
                SelectedProjectChanged(name);
        }

    }
}
