﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using Chemicals;
//using pwiz.CLI.analysis;
//using pwiz.CLI.cv;
//using pwiz.CLI.msdata;

namespace MSFeature
{
    public  class Functions
    {
        /// assuming the mass you give already contains one porton
        public static double MZ(double mass, int charge)
        {
            return (mass + (charge - 1)*Molecules.Proton)/charge;
        }


        public static double ppmToDa(double ppm, double mz)
        {
            double factor = ppm/1000000;
            double da = mz*factor;
            return da;
        }


        

        public static string GetScanNumber(string specid)
        {
            string no = "";
            string[] info = specid.Split(' ');

            foreach (string detail in info)
            {
                if (detail.StartsWith("scan="))
                {
                    no = detail.Substring(5);
                }
            }
            return no;
        }

        



        public static Dictionary<double, int> BinningData(List<double> datapoints,double interval)
        {
            datapoints.Sort();
            //calculate max and min number
            double max = datapoints[datapoints.Count-1];
            double min = datapoints[0];
            return BinningData(datapoints,interval,max,min);
        }


        public static double ConverToBinIdx(double value, double interval)
        {
            double binIdx = 0d;
            if (interval > 1)
            {
                binIdx = (int)((value / interval) * interval);
            }
            else
            {
                int factor = (int)(1 / interval);
                //binIdx = (value/factor)*interval;
                //if (value < 1)
                //    Console.Write("");
                binIdx = Math.Floor(value * factor) * interval;

            }
            return binIdx;
        }

        public static Dictionary<double, int> BinningData(List<double> datapoints, double interval, double max, double min)
        {
            Dictionary<double, int> binnedData = new Dictionary<double, int>();

            foreach (double data in datapoints)
            {
                double idx = ConverToBinIdx(data, interval);
            }
            return binnedData;
        }

        
    }
}
