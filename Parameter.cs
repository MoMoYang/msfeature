﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSFeature
{
    public static class Parameter
    {
        
        public static double Pep_Tolerance = 10;
        public static Units Tolerance_Unit= Units.ppm;
        public static decimal q_value_cutoff = 0.01m;
        public static double RT_difference = 0.1; // this is 0.1 mins
        public static double isolation_window = 3d;
        public static double RT_max_cutoff = 90d;
        public static double RT_min_cutoff = 0d;
        // this is in second
        public static double ElutingRTMin = 2d;
        public enum Units
        {
            ppm,
            Da
        }
    }


}
