﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using Chemicals;
using FileReader;
using PeptideFeature;

namespace MSFeature
{
    class Output
    {

        public static void PrecursorCount(Project project, string filepath)
        {

            StreamWriter sw = new StreamWriter(filepath);
            Dictionary<int, int> precCount = new Dictionary<int, int>();
            sw.WriteLine("# precursor\t#scan");
            
            foreach (IsolatedRegion ms2 in project.FeatureManager.IsolatedRegions())
            {
                if (!precCount.ContainsKey(ms2.CoIsolatedIons.Count))
                {
                    precCount.Add(ms2.CoIsolatedIons.Count,0);
                    if (ms2.CoIsolatedIons.Count >= 9)
                        Console.WriteLine(ms2.ScanNo+"\t"+ms2.CoIsolatedIons.Count);

                }
                precCount[ms2.CoIsolatedIons.Count]++;
            }
            foreach (int prec in precCount.Keys)
            {
                sw.WriteLine(prec+"\t"+precCount[prec]);
            }
            sw.Close();
        }

        public static void OuptputDataSeies(Dictionary<string, List<double>> dataseries,string filepath)
        {
            List<string> datakeys = new List<string>(dataseries.Keys);
            int maxidx = 0;
            StreamWriter sw = new StreamWriter(filepath);

            for (int idx = 0; idx < datakeys.Count; idx++)
            {
                string dataname = datakeys[idx];
                sw.Write(dataname + ',');
                if (dataseries[dataname].Count > maxidx)
                    maxidx = dataseries[dataname].Count;
            }
            sw.WriteLine();

            for (int i = 0; i < maxidx; i++)
            {

                for (int idx = 0; idx < datakeys.Count; idx++)
                {
                    if (i < dataseries[datakeys[idx]].Count)
                        sw.Write(dataseries[datakeys[idx]][i] + ",");
                    else
                        sw.Write(",");
                }
                sw.WriteLine();
            }
            sw.Close();

        }

        public static void ProjectStatistics(List<Project> projectList, string filepath)
        {

            Dictionary<string,int> intersection = new Dictionary<string, int>();
            //Dictionary<string, int> total = new Dictionary<string, int>();
            //foreach (string pep in  projectList[2].FeatureManager.SearchResult.PeptideIds.Keys)
            //{
            //    if (projectList[3].FeatureManager.SearchResult.PeptideIds.ContainsKey(pep))
            //    {
            //        intersection.Add(pep,2);
            //    }
            //    total.Add(pep,1);
            //}
            //foreach (string pep in  projectList[3].FeatureManager.SearchResult.PeptideIds.Keys)
            //{
            //    if (!total.ContainsKey(pep))
            //    {
            //        total.Add(pep,1);
            //    }
            //}
            //double repro = Math.Round(intersection.Keys.Count/(double)total.Keys.Count,4)*100;

            //Console.WriteLine("Total: "+total.Keys.Count+"\nIntersection:"+intersection.Keys.Count+"\nReproducibility:"+repro);


            //StreamReader sw = new StreamReader(@"D:\Lab\Data\20140610_DDA_DIA_DDAp\ProjectStatistics");
            StreamWriter sw = new StreamWriter(filepath);
            string title = "Name\t#Total Features\t#Analyzed Features\t#Identified Features\tAnalyze/Total(%)\t" +
                           "Identified/Total(%)\t#MS1 scan\t#Instrument MS1\t#Periodic MS1\tCustom MS1\t#MS2 scans\t#Instrument MS2\t#Custom MS2\t" +
                           "#Matched MS2 scans\t#NoMatched MS2 scans\tMS1 Rate(Hz)\tMS2 Rate(Hz)\tMS1 scan time\tMS2 scan time\t" +
                           "#Identified PSMs\t#Identified Scans\t#Identified Peptides\t" +
                           "#Average Elution Time\t#scans per feature\tMax\tMin";
            sw.WriteLine(title);

            List<Project> ddap = new List<Project>();

            foreach (Project project in projectList)
            {
                if (project.ID.Contains("DDAp"))
                    ddap.Add(project);

                OutputPeptideList(project, Path.GetDirectoryName(filepath) + @"/" + project.ID + "_peptide_id");
                FeatureManager feature = project.FeatureManager;
                List<PepFeature> analyzed = new List<PepFeature>();
                List<PepFeature> detected = new List<PepFeature>();
                List<PepFeature> identified = new List<PepFeature>();
                double rt = Parameter.ElutingRTMin;
                foreach (PepFeature pep in feature.PeptideFeatures(Parameter.RT_min_cutoff,Parameter.RT_max_cutoff))
                {
                    if (pep.Charge == 1 || pep.Charge == 5)
                        continue;
                    if (feature.IsAnalyzed(pep))
                        analyzed.Add(pep);
                    if (feature.IsIdentified(pep))
                        identified.Add(pep);

                    detected.Add(pep);
                }
                double percentage = analyzed.Count / (double)detected.Count;
                percentage = Math.Round(percentage * 100, 2);

                StringBuilder sb = new StringBuilder();
                sb.Append(feature.Title + "\t" + detected.Count.ToString() + "\t");
                sb.Append(analyzed.Count.ToString() + "\t");
                sb.Append(identified.Count.ToString()+"\t"); 
                sb.Append(percentage.ToString() + "\t");

                percentage = Math.Round(identified.Count/(double) detected.Count,4);
                percentage = percentage*100;
                sb.Append(percentage.ToString() + "\t");

                RawDataInfo rawinfo = project.RawInfo;
                if (rawinfo != null)
                {
                    sb.Append(rawinfo.Statistics.NumMSoneScan.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.NumInstrumentMSoneScan.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.NumPeriodicMSoneScan.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.NumCustomMSoneScan.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.NumMStwoScan.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.NumInstrumentMStwoScan.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.NumCustomMStwoScan.ToString() + "\t");
                }
                else
                    sb.Append("\t\t\t\t\t\t\t");

                sb.Append(feature.Statistics.NoOfScans.ToString() + "\t");
                sb.Append(project.NumboerOfNoMatchedMStwo.ToString() + "\t");

                if (rawinfo != null)
                {
                    sb.Append(rawinfo.Statistics.AverageMS1ScanRate.ToString()+"\t");
                    sb.Append(rawinfo.Statistics.AverageMS2ScanRate.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.AveargeMS1Time.ToString() + "\t");
                    sb.Append(rawinfo.Statistics.AveargeMS2Time.ToString() + "\t");
                    //sb.Append(rawinfo.Statistics.AverageTimePerCycle.ToString() + "\t");
                    //sb.Append(rawinfo.Statistics.NoBinsPerCycle.ToString() + "\t");
                }
                else
                {
                    sb.Append("\t\t\t\t");
                }


                sb.Append(feature.Statistics.IdentifiedPSMs.ToString() + "\t");
                sb.Append(feature.Statistics.IdentifiedScans.ToString() + "\t");
                sb.Append(feature.Statistics.IdentifiedPeptides.ToString() + "\t");

                sb.Append(feature.Statistics.AverageElutionTimePerPep.ToString() + "\t");
                sb.Append(feature.Statistics.NumberOfSpectraPerPep.ToString() + "\t");
                sb.Append(feature.Statistics.MaxNoSpectraOneFeature.ToString() + "\t");
                sb.AppendLine(feature.Statistics.MinNoSpectraOneFeature.ToString());
                sw.Write(sb.ToString());
            }

            #region right 
            //StreamWriter sw = new StreamWriter(folder);
            //string title = "Name\t#Total Features\t#Analyzed Features\t#Targeted Features\tAnalyze/Total(%)\t#MS1 scan\t" +
            //               "#MS2 scans\t#Matched MS2 scans\t#NoMatched MS2 scans\t#Average Elution Time\t#scans per feature\tMax\tMin";
            //sw.WriteLine(title);
            //foreach (Project project in projectList)
            //{
            //    FeatureManager feature = project.FeatureManager;

            //    double percentage = feature.Statistics.AnalyzedFeatures/(double) feature.Statistics.DetectedFeatures;
            //    percentage = Math.Round(percentage*100, 2);

            //    StringBuilder sb = new StringBuilder();
            //    sb.Append(feature.Title + "\t" + feature.Statistics.DetectedFeatures.ToString() + "\t");
            //    sb.Append(feature.Statistics.AnalyzedFeatures.ToString() + "\t");
            //    sb.Append(feature.Statistics.TargetedFeatures.ToString() + "\t");
            //    sb.Append(percentage.ToString() + "\t");

            //    RawDataInfo rawinfo = project.RawInfo;
            //    if (rawinfo != null)
            //    {
            //        sb.Append(rawinfo.Statistics.NumMSoneScan.ToString() + "\t");
            //        sb.Append(rawinfo.Statistics.NumMStwoScan.ToString() + "\t");
            //    }
            //    else
            //        sb.Append("\t\t");

            //    sb.Append(feature.Statistics.NoOfScans.ToString() + "\t");
            //    sb.Append(project.NumboerOfNoMatchedMStwo.ToString() + "\t");
            //    sb.Append(feature.Statistics.AverageElutionTimePerPep.ToString() + "\t");
            //    sb.Append(feature.Statistics.NumberOfSpectraPerPep.ToString() + "\t");
            //    sb.Append(feature.Statistics.MaxNoSpectraOneFeature.ToString() + "\t");
            //    sb.AppendLine(feature.Statistics.MinNoSpectraOneFeature.ToString());

            //    sw.Write(sb.ToString());

            //}
            #endregion

            sw.Close();
            
            string output = Path.GetDirectoryName(filepath);
            OutputBinAndDutyCycles(ddap, output);
            

        }

        private static void OutputPeptideList(Project pro, string filepath)
        {
            StreamWriter sw = new StreamWriter(filepath);

            

            foreach (string pep in pro.FeatureManager.SearchResult.PeptideIds.Keys)
            {
                sw.WriteLine(pep+"\t"+pro.FeatureManager.SearchResult.PeptideIds[pep]);
            }
            sw.Close();
            
        }

        private static void OutputBinAndDutyCycles(List<Project> DDAp ,string folder)
        {
            foreach (Project pro in DDAp)
            {
                if (pro.RawInfo == null)
                    continue;
                string filepath = folder + @"\" + pro.ID+"_dutycycle";
                StreamWriter sw = new StreamWriter(filepath);
                sw.WriteLine("Cycel Starting Time\t#Bin in Cycle\tCycle Time");
                
                for (int idx = 0; idx < pro.RawInfo.CycleTimepoint.Count; idx++)
                {
                    sw.WriteLine(pro.RawInfo.CycleTimepoint[idx]+"\t"+pro.RawInfo.BinsInCycle[idx]+"\t"+pro.RawInfo.DutyCycles[idx]);
                }
                sw.Close();
            }
        }


        public static void OutputIDFeatureAbundance(string outfilepath, ProjectManager projects)
        {
            StreamWriter sw = new StreamWriter(outfilepath);
            
            // this is feature ( one feature is actually be identified more than once with different sequence )
            // raw filename \t peptide sequnece \t charge \t number of scans \t mass error\t summed intensity\t rt_start \t re_end
            sw.WriteLine("rawfile\tFeature index\tpeptide sequence\tcharge state\tFeature M+H\tnumber of identified scans\tsummed intensity\tRT_start\tRT_end");

            foreach (Project project in projects.AllProjects)
            {
                int idx = 0;
                double checkdif = double.MinValue;
                foreach (PepFeature pep in project.FeatureManager.IdentifiedFeatures())
                {
                    idx++;
                    double mass = pep.MonoIsotopicMass + Molecules.Proton;
                    Dictionary<string, List<PSMRecord>> seq2psm = new Dictionary<string, List<PSMRecord>>();
                    int NoIDscans = 0;
                    foreach (PSMRecord psm in project.FeatureManager.FeatureToIDPSMs(pep))
                    {
                        if (!seq2psm.ContainsKey(psm.Sequence))
                            seq2psm.Add(psm.Sequence, new List<PSMRecord>());
                        seq2psm[psm.Sequence].Add(psm);
                        NoIDscans++;

                        double dif = Math.Abs(mass - psm.ExpMass);
                        if (checkdif < dif)
                        {
                            checkdif = dif;
                            if (dif>1)
                                Console.WriteLine("check this case");
                        }
                    }

                    //if (seq2psm.Keys.Count > 1)
                    //{
                    //    Console.WriteLine("this feature has been identified as two different peptides");
                    //}
                    
                    foreach (string seq in seq2psm.Keys)
                    {
                        sw.WriteLine(project.ID + "\t" + idx.ToString() + "\t" + seq + "\t" + pep.Charge + "\t" + mass.ToString() + "\t" + seq2psm[seq].Count.ToString() +"\t"+ pep.SummedIntensity.ToString() + "\t" + pep.RT[0].ToString() + "\t" + pep.RT[1].ToString());
                    }
                }
                Console.WriteLine("Max Mass difference in file "+project.ID+": "+checkdif.ToString());
            }
            sw.Close();
        }


        public static void FlatPeptideFiles(string outfilepath, FeatureManager featureManager,bool idonly)
        {

            StreamWriter sw = new StreamWriter(outfilepath);
            string deliminator = "\t";
            if (idonly)
            {
                foreach (IsolatedRegion scan in featureManager.IdentifiedScans())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(scan.ScanNo + deliminator); // id
                    PepFeature feature = featureManager.TargetFeature(scan);
                    sb.Append(feature.BestTime + deliminator); //rt
                    sb.Append(feature.BaseMz + deliminator); // mz
                    sb.Append("" + deliminator); // score
                    sb.Append(""); // sequence
                    sw.WriteLine(sb.ToString());
                }
            }
            else
            {

                foreach (IsolatedRegion scan in featureManager.IsolatedRegions())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(scan.ScanNo + deliminator); // id
                    PepFeature feature = featureManager.TargetFeature(scan);
                    sb.Append(feature.BestTime + deliminator); //rt
                    sb.Append(feature.BaseMz + deliminator); // mz
                    sb.Append("" + deliminator); // score
                    sb.Append(""); // sequence
                    sw.WriteLine(sb.ToString());
                }
            }

            sw.Close();
        }


        public static void RTandIntensityOfTargetFeature(string filepath,FeatureManager featureManager)
        {
            StreamWriter sw = new StreamWriter(filepath);
            Dictionary<IsolatedRegion,int> idscans = new Dictionary<IsolatedRegion, int>();
            foreach (IsolatedRegion scan in featureManager.IdentifiedScans())
                idscans.Add(scan,0);

            string deliminator = "\t";
            sw.WriteLine("scanNo" + deliminator + "id"+deliminator+"target mz" + deliminator + "scan rt" + deliminator +"feature base mz" +deliminator + "feature best rt" + deliminator + "feature best int");
            foreach (IsolatedRegion scan in featureManager.IsolatedRegions())
            {
                PepFeature feature = featureManager.TargetFeature(scan);
                bool id = false;
                if (idscans.ContainsKey(scan))
                    id = true;
                sw.Write(scan.ScanNo + deliminator + id.ToString()+deliminator+scan.TargetPrecursorMz + deliminator + scan.RetentionTime);
                sw.WriteLine(deliminator + feature.BaseMz+deliminator+feature.BestTime + deliminator + feature.Intensity);
            }
            sw.Close();
        }

        public static void PIFResult(string filepath,FeatureManager featureManager)
        {
            StreamWriter sw = new StreamWriter(filepath);

            foreach (IsolatedRegion scan in featureManager.IsolatedRegions())
            {
                double pif = 0d;
                if (scan.TotalIntInIsolationWindow > 0 )
                    pif = scan.PrecursorIsotopeInt / scan.TotalIntInIsolationWindow;
                sw.WriteLine(scan.ScanNo+"\t"+scan.PrecursorIsotopeInt+"\t"+scan.TotalIntInIsolationWindow+"\t"+pif.ToString());
            }
            sw.Close();
        }

        public static void PeptideAndSampledTimes(string filepath, FeatureManager featureManger)
        {
            StreamWriter sw = new StreamWriter(filepath);
            foreach (PepFeature pep in  featureManger.PeptideFeatures())
            {
                
                //sw.WriteLine(pep.BaseMz+"\t"+pep.BestTime+"\t"+);
            }
            sw.Close();
        }

        public static void ComparePepIDResults(string filepath, List<Percolator> searchResults )
        {
            StreamWriter sw = new StreamWriter(filepath);
            sw.Write("peptide sequence");
            Dictionary<string,Dictionary<Percolator,int>> peptideSeqs = new Dictionary<string, Dictionary<Percolator, int>>();
            
            foreach (Percolator perc in searchResults)
            {
                sw.Write("\t"+perc.ID);
                foreach (PSMRecord psm in perc.PSMs)
                {
                    if (psm.q_value <= Parameter.q_value_cutoff)
                    {
                        if (!peptideSeqs.ContainsKey(psm.Sequence))
                            peptideSeqs.Add(psm.Sequence,new Dictionary<Percolator, int>());
                        if (!peptideSeqs[psm.Sequence].ContainsKey(perc))
                            peptideSeqs[psm.Sequence].Add(perc,0);
                        peptideSeqs[psm.Sequence][perc]++;
                    }
                }
            }
            sw.WriteLine();

            foreach (string seq in peptideSeqs.Keys)
            {
                sw.Write(seq);
                foreach (Percolator perc in searchResults)
                {
                    if (!peptideSeqs[seq].ContainsKey(perc))
                        sw.Write("\t0");
                    else
                        sw.Write("\t"+peptideSeqs[seq][perc].ToString());
                }
                sw.WriteLine();
            }
            sw.Close();
        }


    

        public static void ComparePercolatorResults(string filepath, Dictionary<string, Percolator> results)
        {
            Dictionary<int, List<PSMRecord>> scanToPSM = new Dictionary<int, List<PSMRecord>>();
            List<string> fileorder = new List<string>();
            foreach (string filename in results.Keys)
            {
                fileorder.Add(filename);
                foreach (int scanNo in results[filename].scanToPSMs.Keys)
                {
                    if (!scanToPSM.ContainsKey(scanNo))
                        scanToPSM.Add(scanNo,new List<PSMRecord>());
                }
            }

            StreamWriter sw = new StreamWriter(filepath);
            sw.Write("ScanNo");
            for (int i = 0; i < fileorder.Count; i++)
            {
                sw.Write("\t"+fileorder[i]+"_ID\t"+fileorder[i]+"_Mod");
            
            }
            sw.WriteLine();

            foreach (int scanNo in scanToPSM.Keys)
            {
                for (int i = 0; i < fileorder.Count; i++)
                {
                    string filename = fileorder[i];
                    PSMRecord best = results[filename].BestPSM(scanNo);
                    if (best != null && best.q_value <= Parameter.q_value_cutoff)
                        scanToPSM[scanNo].Add(best);
                    else
                        scanToPSM[scanNo].Add(null);
                }
            }

            foreach (int scanNo in scanToPSM.Keys)
            {
                sw.Write(scanNo.ToString());
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < fileorder.Count; i++)
                {
                    // id \t mod
                    if (scanToPSM[scanNo][i] != null)
                    {
                        sb.Append("\tT\t" + scanToPSM[scanNo][i].Modification.ToString());
                        if (scanToPSM[scanNo][i].Modification)
                            Console.WriteLine("hereh");
                    }
                    else
                        sb.Append("\tF\tF");
                }
                sw.WriteLine(sb.ToString());
            }
            sw.Close();

        }
         
    }
}
