﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Chemicals;
using FileReader;
using PeptideFeature;
namespace MSFeature
{
    public class GetPartMS2
    {
        public static void Filter(FeatureManager featureManager)
        {
            foreach (PepFeature feature in featureManager.PeptideFeatures())
            {
                List<IsolatedRegion> scans = featureManager.FeatureToScans(feature);
                foreach (IsolatedRegion scan in scans)
                {
                    //if (scan.IdentifiedIon == null)

                }

            }

        }

        public static void UnIDMSSpectra(FeatureManager featureManager, string filepath)
        {
            //Dictionary<IsolatedRegion, int> ScanToNo = new Dictionary<IsolatedRegion, int>();
            Dictionary<int,bool> scanIDMap = new Dictionary<int,bool>();
            foreach (IsolatedRegion scan in featureManager.IdentifiedScans())
                scanIDMap.Add(scan.ScanNo,true);
            foreach (IsolatedRegion scan in featureManager.IsolatedRegions())
            {
                if (!scanIDMap.ContainsKey(scan.ScanNo))
                    scanIDMap.Add(scan.ScanNo, false);
            }

            string outfile = Path.GetDirectoryName(filepath) + @"\" + Path.GetFileNameWithoutExtension(filepath) + ".z.noid.ms2";
            StreamWriter sw = new StreamWriter(outfile);
            StreamReader sr = new StreamReader(filepath);
            bool skip = false;
            bool writez = false;
            double precursorMZ = 0d;
            //StringBuilder sb = new StringBuilder();
            int count = 0;
            while (sr.Peek() != -1)
            {
                string oneline =  sr.ReadLine().Trim();
                string[] info = oneline.Split('\t');
                
                if (info[0] == "S")
                {
                    int ScanNo = int.Parse(info[1]);
                    precursorMZ = double.Parse(info[3]);
                    if (!scanIDMap[ScanNo]) 
                        skip = false; // no id
                    else
                        skip = true; // id
                }

                
                if (!skip)
                {
                    if (info[0] == "Z")
                    {
                        writez = true;
                        continue;
                    }

                    if (writez)
                    {
                        StringBuilder sb = new StringBuilder();
                        for (int z = 1; z <= 5; z++)
                        {
                            double mz = precursorMZ * z - ((z - 1) * Molecules.Proton);
                            sb.AppendLine("Z\t" + z.ToString() + "\t" + mz.ToString());
                        }
                        sw.Write(sb.ToString());
                        //Console.WriteLine(sb.ToString());
                        writez = false;
                    }
                    sw.WriteLine(oneline);
                    count++;
                    if (count % 1000 == 0)
                        sw.Flush();
                }
            }
            sw.Close();
            sr.Close();
        }



    }
}
