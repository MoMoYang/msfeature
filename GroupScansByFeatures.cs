﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FileReader;
using PeptideFeature;

namespace MSFeature
{
    public class GroupScansByFeatures
    {


        public static void AssignGroup(FeatureManager featureManager)
        {
            Dictionary<PepFeature,List<IsolatedRegion>> idScans = new Dictionary<PepFeature, List<IsolatedRegion>>();
            Dictionary<PepFeature,List<IsolatedRegion>> noIdScans = new Dictionary<PepFeature, List<IsolatedRegion>>();

            Dictionary<PepFeature,List<IsolatedRegion>> groupByTargetFeature = new Dictionary<PepFeature, List<IsolatedRegion>>();

            int totalIdScans = 0;

            foreach (IsolatedRegion scan in featureManager.IsolatedRegions())
            {
                PepFeature pep = featureManager.TargetFeature(scan);
                if (!groupByTargetFeature.ContainsKey(pep))
                    groupByTargetFeature.Add(pep,new List<IsolatedRegion>());
                groupByTargetFeature[pep].Add(scan);

                if (featureManager.IsIdentified(scan))
                {
                    totalIdScans++;
                    if (!idScans.ContainsKey(pep))
                        idScans.Add(pep, new List<IsolatedRegion>());
                    idScans[pep].Add(scan);

                }
                else
                {
                    if (!noIdScans.ContainsKey(pep))
                        noIdScans.Add(pep, new List<IsolatedRegion>());
                    noIdScans[pep].Add(scan);
                }
            }

            
            int totalNoIdScans = 0;
            int NoIDScanButIDFeature = 0;

            foreach (PepFeature pep in noIdScans.Keys)
            {
                totalNoIdScans += noIdScans[pep].Count;
                if (idScans.ContainsKey(pep))
                {
                    NoIDScanButIDFeature += noIdScans[pep].Count;
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Total id scans: " + totalIdScans.ToString());
            sb.AppendLine("Total no id scans: " + totalNoIdScans.ToString());
            sb.AppendLine("scan is not id but the feature is id: " + NoIDScanButIDFeature.ToString());
            sb.AppendLine("id features: " + idScans.Keys.Count.ToString());
            sb.AppendLine("No id features: " + noIdScans.Keys.Count.ToString());
            
            Console.WriteLine(sb.ToString());
            MessageBox.Show(sb.ToString());
            
        }




    }
}
