﻿namespace MSFeature
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.openKronicResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPercolatorResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openMS2FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openNoMatchedMS2FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openRawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openHardklorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.countToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickTestKronikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateAccuracyOfSamplingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton_project = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Statistics = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.outputTargetFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputIdFeaturesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getUnMatchedMS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIFToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outputSampledPeptideFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputProjectStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputIdentificationComparisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputPeptideFeatureAbundanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputPrecursorCountDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.featureCountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.count234PeptideFeatureOnlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputFeatureInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputFeatureNumbersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outputFeatureDetailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featurePlotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureChargeDisOverMzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureChargeDisOverTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureMzPlotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampledMzRegionPlotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureNumberAcrossRTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureRTDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureRTCrossMZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mS1OverTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mS2AcrossRTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analysisTimesDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dPlotOfFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featuresAndMS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distributionInIsolationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlyTargetedIonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distributionAmongFeatureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlyTargetedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityVsNumbersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureIntensityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ionDistributionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abundanceDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intensityIdDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.precursorIonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.differentChargesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peptdieFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.differentChargesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.chargesOnMS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargeVsIdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureIntensityVsSpectraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.identificationOverTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.identificationAcrossMzBinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scansToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peptidesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proteinsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargeComparisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseOnRawFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseOnKronikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.coisolationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.precursorVsIdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ratiosVsIdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compareTwoProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plotAllFiguresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iTVsChargeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allPrecursorIntensityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dynamicRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton5 = new System.Windows.Forms.ToolStripDropDownButton();
            this.curmulativeCurveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._msPlot = new MSFeature.MSPlot();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.toolStripButton_project,
            this.toolStripButton_Statistics,
            this.toolStripDropDownButton3,
            this.toolStripDropDownButton4,
            this.toolStripDropDownButton5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(887, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openKronicResultToolStripMenuItem,
            this.openPercolatorResultToolStripMenuItem,
            this.openMS2FileToolStripMenuItem,
            this.openNoMatchedMS2FileToolStripMenuItem,
            this.openRawToolStripMenuItem,
            this.openHardklorToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.toolStripDropDownButton1.Text = "File";
            // 
            // openKronicResultToolStripMenuItem
            // 
            this.openKronicResultToolStripMenuItem.Name = "openKronicResultToolStripMenuItem";
            this.openKronicResultToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openKronicResultToolStripMenuItem.Text = "Open Kronik Result";
            this.openKronicResultToolStripMenuItem.Click += new System.EventHandler(this.openKronicResultToolStripMenuItem_Click);
            // 
            // openPercolatorResultToolStripMenuItem
            // 
            this.openPercolatorResultToolStripMenuItem.Name = "openPercolatorResultToolStripMenuItem";
            this.openPercolatorResultToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openPercolatorResultToolStripMenuItem.Text = "Open Percolator Result";
            this.openPercolatorResultToolStripMenuItem.Click += new System.EventHandler(this.openPercolatorResultToolStripMenuItem_Click);
            // 
            // openMS2FileToolStripMenuItem
            // 
            this.openMS2FileToolStripMenuItem.Name = "openMS2FileToolStripMenuItem";
            this.openMS2FileToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openMS2FileToolStripMenuItem.Text = "Open MS2 File (or ms2.title)";
            this.openMS2FileToolStripMenuItem.Click += new System.EventHandler(this.openMS2FileToolStripMenuItem_Click);
            // 
            // openNoMatchedMS2FileToolStripMenuItem
            // 
            this.openNoMatchedMS2FileToolStripMenuItem.Name = "openNoMatchedMS2FileToolStripMenuItem";
            this.openNoMatchedMS2FileToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openNoMatchedMS2FileToolStripMenuItem.Text = "Open NoMatched MS2 File";
            this.openNoMatchedMS2FileToolStripMenuItem.Click += new System.EventHandler(this.openNoMatchedMS2FileToolStripMenuItem_Click);
            // 
            // openRawToolStripMenuItem
            // 
            this.openRawToolStripMenuItem.Name = "openRawToolStripMenuItem";
            this.openRawToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openRawToolStripMenuItem.Text = "Open Raw";
            this.openRawToolStripMenuItem.Click += new System.EventHandler(this.openRawToolStripMenuItem_Click);
            // 
            // openHardklorToolStripMenuItem
            // 
            this.openHardklorToolStripMenuItem.Name = "openHardklorToolStripMenuItem";
            this.openHardklorToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openHardklorToolStripMenuItem.Text = "Open Hardklor";
            this.openHardklorToolStripMenuItem.Click += new System.EventHandler(this.openHardklorToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.countToolStripMenuItem,
            this.featureAnalysisToolStripMenuItem,
            this.pIFToolStripMenuItem,
            this.testToolStripMenuItem,
            this.quickTestKronikToolStripMenuItem,
            this.calculateAccuracyOfSamplingToolStripMenuItem});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(67, 22);
            this.toolStripDropDownButton2.Text = "Function";
            // 
            // countToolStripMenuItem
            // 
            this.countToolStripMenuItem.Name = "countToolStripMenuItem";
            this.countToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.countToolStripMenuItem.Text = "Generate FeatureManager";
            this.countToolStripMenuItem.Click += new System.EventHandler(this.countToolStripMenuItem_Click);
            // 
            // featureAnalysisToolStripMenuItem
            // 
            this.featureAnalysisToolStripMenuItem.Name = "featureAnalysisToolStripMenuItem";
            this.featureAnalysisToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.featureAnalysisToolStripMenuItem.Text = "FeatureAnalysis";
            // 
            // pIFToolStripMenuItem
            // 
            this.pIFToolStripMenuItem.Name = "pIFToolStripMenuItem";
            this.pIFToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.pIFToolStripMenuItem.Text = "PIF";
            this.pIFToolStripMenuItem.Click += new System.EventHandler(this.pIFToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.testToolStripMenuItem.Text = "Group Scans by features";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // quickTestKronikToolStripMenuItem
            // 
            this.quickTestKronikToolStripMenuItem.Name = "quickTestKronikToolStripMenuItem";
            this.quickTestKronikToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.quickTestKronikToolStripMenuItem.Text = "Quick test Kronik";
            this.quickTestKronikToolStripMenuItem.Click += new System.EventHandler(this.quickTestKronikToolStripMenuItem_Click);
            // 
            // calculateAccuracyOfSamplingToolStripMenuItem
            // 
            this.calculateAccuracyOfSamplingToolStripMenuItem.Name = "calculateAccuracyOfSamplingToolStripMenuItem";
            this.calculateAccuracyOfSamplingToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.calculateAccuracyOfSamplingToolStripMenuItem.Text = "Calculate accuracy of sampling";
            this.calculateAccuracyOfSamplingToolStripMenuItem.Click += new System.EventHandler(this.calculateAccuracyOfSamplingToolStripMenuItem_Click);
            // 
            // toolStripButton_project
            // 
            this.toolStripButton_project.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_project.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_project.Image")));
            this.toolStripButton_project.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_project.Name = "toolStripButton_project";
            this.toolStripButton_project.Size = new System.Drawing.Size(53, 22);
            this.toolStripButton_project.Text = "Projects";
            this.toolStripButton_project.Click += new System.EventHandler(this.toolStripButton_project_Click);
            // 
            // toolStripButton_Statistics
            // 
            this.toolStripButton_Statistics.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Statistics.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Statistics.Image")));
            this.toolStripButton_Statistics.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Statistics.Name = "toolStripButton_Statistics";
            this.toolStripButton_Statistics.Size = new System.Drawing.Size(57, 22);
            this.toolStripButton_Statistics.Text = "Statistics";
            this.toolStripButton_Statistics.Click += new System.EventHandler(this.toolStripButtonStatistics_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outputTargetFeaturesToolStripMenuItem,
            this.outputIdFeaturesToolStripMenuItem1,
            this.outputToolStripMenuItem,
            this.getUnMatchedMS2ToolStripMenuItem,
            this.pIFToolStripMenuItem1,
            this.outputSampledPeptideFeaturesToolStripMenuItem,
            this.outputProjectStatisticsToolStripMenuItem,
            this.outputIdentificationComparisionToolStripMenuItem,
            this.outputPeptideFeatureAbundanceToolStripMenuItem,
            this.outputPrecursorCountDistributionToolStripMenuItem,
            this.testToolStripMenuItem1,
            this.featureCountsToolStripMenuItem,
            this.count234PeptideFeatureOnlyToolStripMenuItem,
            this.outputFeatureInfoToolStripMenuItem});
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(58, 22);
            this.toolStripDropDownButton3.Text = "Output";
            // 
            // outputTargetFeaturesToolStripMenuItem
            // 
            this.outputTargetFeaturesToolStripMenuItem.Name = "outputTargetFeaturesToolStripMenuItem";
            this.outputTargetFeaturesToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputTargetFeaturesToolStripMenuItem.Text = "Output Target features";
            this.outputTargetFeaturesToolStripMenuItem.Click += new System.EventHandler(this.outputTargetFeatureToolStripMenuItem_Click);
            // 
            // outputIdFeaturesToolStripMenuItem1
            // 
            this.outputIdFeaturesToolStripMenuItem1.Name = "outputIdFeaturesToolStripMenuItem1";
            this.outputIdFeaturesToolStripMenuItem1.Size = new System.Drawing.Size(275, 22);
            this.outputIdFeaturesToolStripMenuItem1.Text = "Output Id features";
            this.outputIdFeaturesToolStripMenuItem1.Click += new System.EventHandler(this.outputIdFeaturesToolStripMenuItem_Click);
            // 
            // outputToolStripMenuItem
            // 
            this.outputToolStripMenuItem.Name = "outputToolStripMenuItem";
            this.outputToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputToolStripMenuItem.Text = "Output RT and Int of targeted features";
            this.outputToolStripMenuItem.Click += new System.EventHandler(this.outputToolStripMenuItem_Click);
            // 
            // getUnMatchedMS2ToolStripMenuItem
            // 
            this.getUnMatchedMS2ToolStripMenuItem.Name = "getUnMatchedMS2ToolStripMenuItem";
            this.getUnMatchedMS2ToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.getUnMatchedMS2ToolStripMenuItem.Text = "GetUnMatchedMS2";
            this.getUnMatchedMS2ToolStripMenuItem.Click += new System.EventHandler(this.getUnMatchedMS2ToolStripMenuItem_Click);
            // 
            // pIFToolStripMenuItem1
            // 
            this.pIFToolStripMenuItem1.Name = "pIFToolStripMenuItem1";
            this.pIFToolStripMenuItem1.Size = new System.Drawing.Size(275, 22);
            this.pIFToolStripMenuItem1.Text = "PIF";
            this.pIFToolStripMenuItem1.Click += new System.EventHandler(this.pIFToolStripMenuItem1_Click);
            // 
            // outputSampledPeptideFeaturesToolStripMenuItem
            // 
            this.outputSampledPeptideFeaturesToolStripMenuItem.Name = "outputSampledPeptideFeaturesToolStripMenuItem";
            this.outputSampledPeptideFeaturesToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputSampledPeptideFeaturesToolStripMenuItem.Text = "Output Sampled peptide features";
            this.outputSampledPeptideFeaturesToolStripMenuItem.Click += new System.EventHandler(this.outputSampledPeptideFeaturesToolStripMenuItem_Click);
            // 
            // outputProjectStatisticsToolStripMenuItem
            // 
            this.outputProjectStatisticsToolStripMenuItem.Name = "outputProjectStatisticsToolStripMenuItem";
            this.outputProjectStatisticsToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputProjectStatisticsToolStripMenuItem.Text = "Output project statistics";
            this.outputProjectStatisticsToolStripMenuItem.Click += new System.EventHandler(this.outputProjectStatisticsToolStripMenuItem_Click);
            // 
            // outputIdentificationComparisionToolStripMenuItem
            // 
            this.outputIdentificationComparisionToolStripMenuItem.Name = "outputIdentificationComparisionToolStripMenuItem";
            this.outputIdentificationComparisionToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputIdentificationComparisionToolStripMenuItem.Text = "Output identification comparision";
            this.outputIdentificationComparisionToolStripMenuItem.Click += new System.EventHandler(this.outputIdentificationComparisionToolStripMenuItem_Click);
            // 
            // outputPeptideFeatureAbundanceToolStripMenuItem
            // 
            this.outputPeptideFeatureAbundanceToolStripMenuItem.Name = "outputPeptideFeatureAbundanceToolStripMenuItem";
            this.outputPeptideFeatureAbundanceToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputPeptideFeatureAbundanceToolStripMenuItem.Text = "Output peptide feature abundance";
            this.outputPeptideFeatureAbundanceToolStripMenuItem.Click += new System.EventHandler(this.outputPeptideFeatureAbundanceToolStripMenuItem_Click);
            // 
            // outputPrecursorCountDistributionToolStripMenuItem
            // 
            this.outputPrecursorCountDistributionToolStripMenuItem.Name = "outputPrecursorCountDistributionToolStripMenuItem";
            this.outputPrecursorCountDistributionToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputPrecursorCountDistributionToolStripMenuItem.Text = "Output precursor count distribution";
            this.outputPrecursorCountDistributionToolStripMenuItem.Click += new System.EventHandler(this.outputPrecursorCountDistributionToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem1
            // 
            this.testToolStripMenuItem1.Name = "testToolStripMenuItem1";
            this.testToolStripMenuItem1.Size = new System.Drawing.Size(275, 22);
            this.testToolStripMenuItem1.Text = "Test ";
            this.testToolStripMenuItem1.Click += new System.EventHandler(this.testToolStripMenuItem1_Click);
            // 
            // featureCountsToolStripMenuItem
            // 
            this.featureCountsToolStripMenuItem.Name = "featureCountsToolStripMenuItem";
            this.featureCountsToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.featureCountsToolStripMenuItem.Text = "Feature Counts";
            this.featureCountsToolStripMenuItem.Click += new System.EventHandler(this.featureCountsToolStripMenuItem_Click);
            // 
            // count234PeptideFeatureOnlyToolStripMenuItem
            // 
            this.count234PeptideFeatureOnlyToolStripMenuItem.Name = "count234PeptideFeatureOnlyToolStripMenuItem";
            this.count234PeptideFeatureOnlyToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.count234PeptideFeatureOnlyToolStripMenuItem.Text = "Count +2,+3,+4 Peptide feature only";
            this.count234PeptideFeatureOnlyToolStripMenuItem.Click += new System.EventHandler(this.count234PeptideFeatureOnlyToolStripMenuItem_Click);
            // 
            // outputFeatureInfoToolStripMenuItem
            // 
            this.outputFeatureInfoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outputFeatureNumbersToolStripMenuItem1,
            this.outputFeatureDetailsToolStripMenuItem1});
            this.outputFeatureInfoToolStripMenuItem.Name = "outputFeatureInfoToolStripMenuItem";
            this.outputFeatureInfoToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.outputFeatureInfoToolStripMenuItem.Text = "Output feature info";
            // 
            // outputFeatureNumbersToolStripMenuItem1
            // 
            this.outputFeatureNumbersToolStripMenuItem1.Name = "outputFeatureNumbersToolStripMenuItem1";
            this.outputFeatureNumbersToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.outputFeatureNumbersToolStripMenuItem1.Text = "Output feature numbers";
            this.outputFeatureNumbersToolStripMenuItem1.Click += new System.EventHandler(this.outputFeatureNumbersToolStripMenuItem_Click);
            // 
            // outputFeatureDetailsToolStripMenuItem1
            // 
            this.outputFeatureDetailsToolStripMenuItem1.Name = "outputFeatureDetailsToolStripMenuItem1";
            this.outputFeatureDetailsToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.outputFeatureDetailsToolStripMenuItem1.Text = "Output feature details";
            this.outputFeatureDetailsToolStripMenuItem1.Click += new System.EventHandler(this.outputFeatureDetailsToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.featurePlotsToolStripMenuItem,
            this.featureMzPlotToolStripMenuItem,
            this.featureCountToolStripMenuItem,
            this.sampledMzRegionPlotToolStripMenuItem,
            this.featureNumberAcrossRTToolStripMenuItem,
            this.featureRTDistributionToolStripMenuItem,
            this.featureRTCrossMZToolStripMenuItem,
            this.mS1OverTimeToolStripMenuItem,
            this.mS2AcrossRTToolStripMenuItem,
            this.analysisTimesDistributionToolStripMenuItem,
            this.dPlotOfFeaturesToolStripMenuItem,
            this.distributionInIsolationToolStripMenuItem,
            this.distributionAmongFeatureToolStripMenuItem,
            this.intensityVsNumbersToolStripMenuItem,
            this.ionDistributionsToolStripMenuItem,
            this.intensityIdDistributionToolStripMenuItem,
            this.chargeVsIdToolStripMenuItem,
            this.featureIntensityVsSpectraToolStripMenuItem,
            this.identificationOverTimeToolStripMenuItem,
            this.identificationAcrossMzBinToolStripMenuItem,
            this.chargeComparisionToolStripMenuItem,
            this.coisolationToolStripMenuItem,
            this.compareTwoProjectToolStripMenuItem,
            this.plotAllFiguresToolStripMenuItem,
            this.iTVsChargeToolStripMenuItem,
            this.allPrecursorIntensityToolStripMenuItem,
            this.dynamicRangeToolStripMenuItem});
            this.toolStripDropDownButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton4.Image")));
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(41, 22);
            this.toolStripDropDownButton4.Text = "Plot";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // featurePlotsToolStripMenuItem
            // 
            this.featurePlotsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.featureChargeDisOverMzToolStripMenuItem,
            this.featureChargeDisOverTimeToolStripMenuItem});
            this.featurePlotsToolStripMenuItem.Name = "featurePlotsToolStripMenuItem";
            this.featurePlotsToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featurePlotsToolStripMenuItem.Text = "Feature Plots";
            // 
            // featureChargeDisOverMzToolStripMenuItem
            // 
            this.featureChargeDisOverMzToolStripMenuItem.Name = "featureChargeDisOverMzToolStripMenuItem";
            this.featureChargeDisOverMzToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.featureChargeDisOverMzToolStripMenuItem.Text = "Feature charge dis over mz";
            this.featureChargeDisOverMzToolStripMenuItem.Click += new System.EventHandler(this.featureChargeDisOverMzToolStripMenuItem_Click);
            // 
            // featureChargeDisOverTimeToolStripMenuItem
            // 
            this.featureChargeDisOverTimeToolStripMenuItem.Name = "featureChargeDisOverTimeToolStripMenuItem";
            this.featureChargeDisOverTimeToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.featureChargeDisOverTimeToolStripMenuItem.Text = "Feature charge dis over time";
            this.featureChargeDisOverTimeToolStripMenuItem.Click += new System.EventHandler(this.featureChargeDisOverTimeToolStripMenuItem_Click);
            // 
            // featureMzPlotToolStripMenuItem
            // 
            this.featureMzPlotToolStripMenuItem.Name = "featureMzPlotToolStripMenuItem";
            this.featureMzPlotToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featureMzPlotToolStripMenuItem.Text = "Feature mz plot";
            this.featureMzPlotToolStripMenuItem.Click += new System.EventHandler(this.featureMzPlotToolStripMenuItem_Click);
            // 
            // featureCountToolStripMenuItem
            // 
            this.featureCountToolStripMenuItem.Name = "featureCountToolStripMenuItem";
            this.featureCountToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featureCountToolStripMenuItem.Text = "Detect Noise";
            this.featureCountToolStripMenuItem.Click += new System.EventHandler(this.featureCountToolStripMenuItem_Click);
            // 
            // sampledMzRegionPlotToolStripMenuItem
            // 
            this.sampledMzRegionPlotToolStripMenuItem.Name = "sampledMzRegionPlotToolStripMenuItem";
            this.sampledMzRegionPlotToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.sampledMzRegionPlotToolStripMenuItem.Text = "Sampled mz region plot";
            this.sampledMzRegionPlotToolStripMenuItem.Click += new System.EventHandler(this.sampledMzRegionPlotToolStripMenuItem_Click);
            // 
            // featureNumberAcrossRTToolStripMenuItem
            // 
            this.featureNumberAcrossRTToolStripMenuItem.Name = "featureNumberAcrossRTToolStripMenuItem";
            this.featureNumberAcrossRTToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featureNumberAcrossRTToolStripMenuItem.Text = "Feature Number Across RT";
            this.featureNumberAcrossRTToolStripMenuItem.Click += new System.EventHandler(this.featureNumberAcrossRTToolStripMenuItem_Click);
            // 
            // featureRTDistributionToolStripMenuItem
            // 
            this.featureRTDistributionToolStripMenuItem.Name = "featureRTDistributionToolStripMenuItem";
            this.featureRTDistributionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featureRTDistributionToolStripMenuItem.Text = "Feature RT distribution";
            this.featureRTDistributionToolStripMenuItem.Click += new System.EventHandler(this.featureRTDistributionToolStripMenuItem_Click);
            // 
            // featureRTCrossMZToolStripMenuItem
            // 
            this.featureRTCrossMZToolStripMenuItem.Name = "featureRTCrossMZToolStripMenuItem";
            this.featureRTCrossMZToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featureRTCrossMZToolStripMenuItem.Text = "Feature RT Across MZ";
            this.featureRTCrossMZToolStripMenuItem.Click += new System.EventHandler(this.featureRTCrossMZToolStripMenuItem_Click);
            // 
            // mS1OverTimeToolStripMenuItem
            // 
            this.mS1OverTimeToolStripMenuItem.Name = "mS1OverTimeToolStripMenuItem";
            this.mS1OverTimeToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.mS1OverTimeToolStripMenuItem.Text = "MS1 over time";
            this.mS1OverTimeToolStripMenuItem.Click += new System.EventHandler(this.mS1OverTimeToolStripMenuItem_Click);
            // 
            // mS2AcrossRTToolStripMenuItem
            // 
            this.mS2AcrossRTToolStripMenuItem.Name = "mS2AcrossRTToolStripMenuItem";
            this.mS2AcrossRTToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.mS2AcrossRTToolStripMenuItem.Text = "MS2 Across RT";
            this.mS2AcrossRTToolStripMenuItem.Click += new System.EventHandler(this.mS2AcrossRTToolStripMenuItem_Click);
            // 
            // analysisTimesDistributionToolStripMenuItem
            // 
            this.analysisTimesDistributionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalizeToolStripMenuItem});
            this.analysisTimesDistributionToolStripMenuItem.Name = "analysisTimesDistributionToolStripMenuItem";
            this.analysisTimesDistributionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.analysisTimesDistributionToolStripMenuItem.Text = "Analysis times distribution";
            this.analysisTimesDistributionToolStripMenuItem.Click += new System.EventHandler(this.analysisTimesDistributionToolStripMenuItem_Click);
            // 
            // normalizeToolStripMenuItem
            // 
            this.normalizeToolStripMenuItem.Name = "normalizeToolStripMenuItem";
            this.normalizeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.normalizeToolStripMenuItem.Text = "normalize";
            this.normalizeToolStripMenuItem.Click += new System.EventHandler(this.normalizeToolStripMenuItem_Click);
            // 
            // dPlotOfFeaturesToolStripMenuItem
            // 
            this.dPlotOfFeaturesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.featuresAndMS2ToolStripMenuItem,
            this.mS2ToolStripMenuItem});
            this.dPlotOfFeaturesToolStripMenuItem.Name = "dPlotOfFeaturesToolStripMenuItem";
            this.dPlotOfFeaturesToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.dPlotOfFeaturesToolStripMenuItem.Text = "2D plot of features";
            // 
            // featuresAndMS2ToolStripMenuItem
            // 
            this.featuresAndMS2ToolStripMenuItem.Name = "featuresAndMS2ToolStripMenuItem";
            this.featuresAndMS2ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.featuresAndMS2ToolStripMenuItem.Text = "features and MS2";
            this.featuresAndMS2ToolStripMenuItem.Click += new System.EventHandler(this.featuresAndMS2ToolStripMenuItem_Click);
            // 
            // mS2ToolStripMenuItem
            // 
            this.mS2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mzToolStripMenuItem,
            this.rTToolStripMenuItem});
            this.mS2ToolStripMenuItem.Name = "mS2ToolStripMenuItem";
            this.mS2ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.mS2ToolStripMenuItem.Text = "MS2";
            this.mS2ToolStripMenuItem.Click += new System.EventHandler(this.mS2ToolStripMenuItem_Click);
            // 
            // mzToolStripMenuItem
            // 
            this.mzToolStripMenuItem.Name = "mzToolStripMenuItem";
            this.mzToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.mzToolStripMenuItem.Text = "m/z";
            this.mzToolStripMenuItem.Click += new System.EventHandler(this.mzToolStripMenuItem_Click);
            // 
            // rTToolStripMenuItem
            // 
            this.rTToolStripMenuItem.Name = "rTToolStripMenuItem";
            this.rTToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.rTToolStripMenuItem.Text = "RT";
            this.rTToolStripMenuItem.Click += new System.EventHandler(this.rTToolStripMenuItem_Click);
            // 
            // distributionInIsolationToolStripMenuItem
            // 
            this.distributionInIsolationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlyTargetedIonToolStripMenuItem});
            this.distributionInIsolationToolStripMenuItem.Name = "distributionInIsolationToolStripMenuItem";
            this.distributionInIsolationToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.distributionInIsolationToolStripMenuItem.Text = "Distribution in Isolation";
            this.distributionInIsolationToolStripMenuItem.Click += new System.EventHandler(this.distributionInIsolationToolStripMenuItem_Click);
            // 
            // onlyTargetedIonToolStripMenuItem
            // 
            this.onlyTargetedIonToolStripMenuItem.Name = "onlyTargetedIonToolStripMenuItem";
            this.onlyTargetedIonToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.onlyTargetedIonToolStripMenuItem.Text = "only targeted ion";
            this.onlyTargetedIonToolStripMenuItem.Click += new System.EventHandler(this.onlyTargetedIonToolStripMenuItem_Click);
            // 
            // distributionAmongFeatureToolStripMenuItem
            // 
            this.distributionAmongFeatureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlyTargetedToolStripMenuItem});
            this.distributionAmongFeatureToolStripMenuItem.Name = "distributionAmongFeatureToolStripMenuItem";
            this.distributionAmongFeatureToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.distributionAmongFeatureToolStripMenuItem.Text = "Distribution among a feature";
            this.distributionAmongFeatureToolStripMenuItem.Click += new System.EventHandler(this.distributionAmongFeatureToolStripMenuItem_Click);
            // 
            // onlyTargetedToolStripMenuItem
            // 
            this.onlyTargetedToolStripMenuItem.Name = "onlyTargetedToolStripMenuItem";
            this.onlyTargetedToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.onlyTargetedToolStripMenuItem.Text = "only targeted ion";
            this.onlyTargetedToolStripMenuItem.Click += new System.EventHandler(this.onlyTargetedToolStripMenuItem_Click);
            // 
            // intensityVsNumbersToolStripMenuItem
            // 
            this.intensityVsNumbersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.featureIntensityToolStripMenuItem});
            this.intensityVsNumbersToolStripMenuItem.Name = "intensityVsNumbersToolStripMenuItem";
            this.intensityVsNumbersToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.intensityVsNumbersToolStripMenuItem.Text = "Feautre distributions";
            // 
            // featureIntensityToolStripMenuItem
            // 
            this.featureIntensityToolStripMenuItem.Name = "featureIntensityToolStripMenuItem";
            this.featureIntensityToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.featureIntensityToolStripMenuItem.Text = "Intensity distribution";
            this.featureIntensityToolStripMenuItem.Click += new System.EventHandler(this.featureIntensityToolStripMenuItem_Click);
            // 
            // ionDistributionsToolStripMenuItem
            // 
            this.ionDistributionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abundanceDistributionToolStripMenuItem});
            this.ionDistributionsToolStripMenuItem.Name = "ionDistributionsToolStripMenuItem";
            this.ionDistributionsToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.ionDistributionsToolStripMenuItem.Text = "Ion distributions";
            // 
            // abundanceDistributionToolStripMenuItem
            // 
            this.abundanceDistributionToolStripMenuItem.Name = "abundanceDistributionToolStripMenuItem";
            this.abundanceDistributionToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.abundanceDistributionToolStripMenuItem.Text = "Abundance distribution";
            this.abundanceDistributionToolStripMenuItem.Click += new System.EventHandler(this.abundanceDistributionToolStripMenuItem_Click);
            // 
            // intensityIdDistributionToolStripMenuItem
            // 
            this.intensityIdDistributionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.precursorIonsToolStripMenuItem,
            this.peptdieFeaturesToolStripMenuItem,
            this.chargesOnMS2ToolStripMenuItem});
            this.intensityIdDistributionToolStripMenuItem.Name = "intensityIdDistributionToolStripMenuItem";
            this.intensityIdDistributionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.intensityIdDistributionToolStripMenuItem.Text = "Intensity vs id distribution";
            // 
            // precursorIonsToolStripMenuItem
            // 
            this.precursorIonsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.differentChargesToolStripMenuItem});
            this.precursorIonsToolStripMenuItem.Name = "precursorIonsToolStripMenuItem";
            this.precursorIonsToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.precursorIonsToolStripMenuItem.Text = "precursor ions";
            this.precursorIonsToolStripMenuItem.Click += new System.EventHandler(this.precursorIonsToolStripMenuItem_Click);
            // 
            // differentChargesToolStripMenuItem
            // 
            this.differentChargesToolStripMenuItem.Name = "differentChargesToolStripMenuItem";
            this.differentChargesToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.differentChargesToolStripMenuItem.Text = "different charges";
            this.differentChargesToolStripMenuItem.Click += new System.EventHandler(this.differentChargesToolStripMenuItem_Click);
            // 
            // peptdieFeaturesToolStripMenuItem
            // 
            this.peptdieFeaturesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.differentChargesToolStripMenuItem1});
            this.peptdieFeaturesToolStripMenuItem.Name = "peptdieFeaturesToolStripMenuItem";
            this.peptdieFeaturesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.peptdieFeaturesToolStripMenuItem.Text = "peptdie features";
            this.peptdieFeaturesToolStripMenuItem.Click += new System.EventHandler(this.peptdieFeaturesToolStripMenuItem_Click);
            // 
            // differentChargesToolStripMenuItem1
            // 
            this.differentChargesToolStripMenuItem1.Name = "differentChargesToolStripMenuItem1";
            this.differentChargesToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.differentChargesToolStripMenuItem1.Text = "different charges";
            this.differentChargesToolStripMenuItem1.Click += new System.EventHandler(this.differentChargesToolStripMenuItem1_Click);
            // 
            // chargesOnMS2ToolStripMenuItem
            // 
            this.chargesOnMS2ToolStripMenuItem.Name = "chargesOnMS2ToolStripMenuItem";
            this.chargesOnMS2ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.chargesOnMS2ToolStripMenuItem.Text = "charges on MS2";
            this.chargesOnMS2ToolStripMenuItem.Click += new System.EventHandler(this.chargesOnMS2ToolStripMenuItem_Click);
            // 
            // chargeVsIdToolStripMenuItem
            // 
            this.chargeVsIdToolStripMenuItem.Name = "chargeVsIdToolStripMenuItem";
            this.chargeVsIdToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.chargeVsIdToolStripMenuItem.Text = "Precursor charge vs id";
            this.chargeVsIdToolStripMenuItem.Click += new System.EventHandler(this.chargeVsIdToolStripMenuItem_Click);
            // 
            // featureIntensityVsSpectraToolStripMenuItem
            // 
            this.featureIntensityVsSpectraToolStripMenuItem.Name = "featureIntensityVsSpectraToolStripMenuItem";
            this.featureIntensityVsSpectraToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.featureIntensityVsSpectraToolStripMenuItem.Text = "Feature intensity vs # spectra";
            this.featureIntensityVsSpectraToolStripMenuItem.Click += new System.EventHandler(this.featureIntensityVsSpectraToolStripMenuItem_Click);
            // 
            // identificationOverTimeToolStripMenuItem
            // 
            this.identificationOverTimeToolStripMenuItem.Name = "identificationOverTimeToolStripMenuItem";
            this.identificationOverTimeToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.identificationOverTimeToolStripMenuItem.Text = "Identification over time";
            this.identificationOverTimeToolStripMenuItem.Click += new System.EventHandler(this.identificationOverTimeToolStripMenuItem_Click);
            // 
            // identificationAcrossMzBinToolStripMenuItem
            // 
            this.identificationAcrossMzBinToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scansToolStripMenuItem,
            this.peptidesToolStripMenuItem,
            this.proteinsToolStripMenuItem});
            this.identificationAcrossMzBinToolStripMenuItem.Name = "identificationAcrossMzBinToolStripMenuItem";
            this.identificationAcrossMzBinToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.identificationAcrossMzBinToolStripMenuItem.Text = "Identification across m/z bin";
            // 
            // scansToolStripMenuItem
            // 
            this.scansToolStripMenuItem.Name = "scansToolStripMenuItem";
            this.scansToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.scansToolStripMenuItem.Text = "scans";
            this.scansToolStripMenuItem.Click += new System.EventHandler(this.scansToolStripMenuItem_Click);
            // 
            // peptidesToolStripMenuItem
            // 
            this.peptidesToolStripMenuItem.Name = "peptidesToolStripMenuItem";
            this.peptidesToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.peptidesToolStripMenuItem.Text = "peptides";
            this.peptidesToolStripMenuItem.Click += new System.EventHandler(this.peptidesToolStripMenuItem_Click);
            // 
            // proteinsToolStripMenuItem
            // 
            this.proteinsToolStripMenuItem.Name = "proteinsToolStripMenuItem";
            this.proteinsToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.proteinsToolStripMenuItem.Text = "proteins";
            this.proteinsToolStripMenuItem.Click += new System.EventHandler(this.proteinsToolStripMenuItem_Click);
            // 
            // chargeComparisionToolStripMenuItem
            // 
            this.chargeComparisionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baseOnRawFileToolStripMenuItem,
            this.baseOnKronikToolStripMenuItem,
            this.outputToolStripMenuItem1});
            this.chargeComparisionToolStripMenuItem.Name = "chargeComparisionToolStripMenuItem";
            this.chargeComparisionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.chargeComparisionToolStripMenuItem.Text = "Charge Comparision";
            // 
            // baseOnRawFileToolStripMenuItem
            // 
            this.baseOnRawFileToolStripMenuItem.Name = "baseOnRawFileToolStripMenuItem";
            this.baseOnRawFileToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.baseOnRawFileToolStripMenuItem.Text = "base on raw file";
            this.baseOnRawFileToolStripMenuItem.Click += new System.EventHandler(this.baseOnRawFileToolStripMenuItem_Click);
            // 
            // baseOnKronikToolStripMenuItem
            // 
            this.baseOnKronikToolStripMenuItem.Name = "baseOnKronikToolStripMenuItem";
            this.baseOnKronikToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.baseOnKronikToolStripMenuItem.Text = "base on kronik";
            this.baseOnKronikToolStripMenuItem.Click += new System.EventHandler(this.baseOnKronikToolStripMenuItem_Click);
            // 
            // outputToolStripMenuItem1
            // 
            this.outputToolStripMenuItem1.Name = "outputToolStripMenuItem1";
            this.outputToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.outputToolStripMenuItem1.Text = "output";
            this.outputToolStripMenuItem1.Click += new System.EventHandler(this.outputToolStripMenuItem1_Click);
            // 
            // coisolationToolStripMenuItem
            // 
            this.coisolationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.precursorVsIdToolStripMenuItem,
            this.ratiosVsIdToolStripMenuItem});
            this.coisolationToolStripMenuItem.Name = "coisolationToolStripMenuItem";
            this.coisolationToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.coisolationToolStripMenuItem.Text = "Coisolation";
            // 
            // precursorVsIdToolStripMenuItem
            // 
            this.precursorVsIdToolStripMenuItem.Name = "precursorVsIdToolStripMenuItem";
            this.precursorVsIdToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.precursorVsIdToolStripMenuItem.Text = "# precursor vs id";
            this.precursorVsIdToolStripMenuItem.Click += new System.EventHandler(this.precursorVsIdToolStripMenuItem_Click);
            // 
            // ratiosVsIdToolStripMenuItem
            // 
            this.ratiosVsIdToolStripMenuItem.Name = "ratiosVsIdToolStripMenuItem";
            this.ratiosVsIdToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ratiosVsIdToolStripMenuItem.Text = "ratios vs id";
            this.ratiosVsIdToolStripMenuItem.Click += new System.EventHandler(this.ratiosVsIdToolStripMenuItem_Click);
            // 
            // compareTwoProjectToolStripMenuItem
            // 
            this.compareTwoProjectToolStripMenuItem.Name = "compareTwoProjectToolStripMenuItem";
            this.compareTwoProjectToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.compareTwoProjectToolStripMenuItem.Text = "Compare two project";
            this.compareTwoProjectToolStripMenuItem.Click += new System.EventHandler(this.compareTwoProjectToolStripMenuItem_Click);
            // 
            // plotAllFiguresToolStripMenuItem
            // 
            this.plotAllFiguresToolStripMenuItem.Name = "plotAllFiguresToolStripMenuItem";
            this.plotAllFiguresToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.plotAllFiguresToolStripMenuItem.Text = "Plot all figures";
            this.plotAllFiguresToolStripMenuItem.Click += new System.EventHandler(this.plotAllFiguresToolStripMenuItem_Click);
            // 
            // iTVsChargeToolStripMenuItem
            // 
            this.iTVsChargeToolStripMenuItem.Name = "iTVsChargeToolStripMenuItem";
            this.iTVsChargeToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.iTVsChargeToolStripMenuItem.Text = "IT vs Charge";
            this.iTVsChargeToolStripMenuItem.Click += new System.EventHandler(this.iTVsChargeToolStripMenuItem_Click);
            // 
            // allPrecursorIntensityToolStripMenuItem
            // 
            this.allPrecursorIntensityToolStripMenuItem.Name = "allPrecursorIntensityToolStripMenuItem";
            this.allPrecursorIntensityToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.allPrecursorIntensityToolStripMenuItem.Text = "all precursor intensity";
            this.allPrecursorIntensityToolStripMenuItem.Click += new System.EventHandler(this.allPrecursorIntensityToolStripMenuItem_Click);
            // 
            // dynamicRangeToolStripMenuItem
            // 
            this.dynamicRangeToolStripMenuItem.Name = "dynamicRangeToolStripMenuItem";
            this.dynamicRangeToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.dynamicRangeToolStripMenuItem.Text = "Dynamic Range";
            this.dynamicRangeToolStripMenuItem.Click += new System.EventHandler(this.dynamicRangeToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton5
            // 
            this.toolStripDropDownButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.curmulativeCurveToolStripMenuItem});
            this.toolStripDropDownButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton5.Image")));
            this.toolStripDropDownButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton5.Name = "toolStripDropDownButton5";
            this.toolStripDropDownButton5.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton5.Text = "toolStripDropDownButton5";
            // 
            // curmulativeCurveToolStripMenuItem
            // 
            this.curmulativeCurveToolStripMenuItem.Name = "curmulativeCurveToolStripMenuItem";
            this.curmulativeCurveToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.curmulativeCurveToolStripMenuItem.Text = "curmulativeCurve";
            this.curmulativeCurveToolStripMenuItem.Click += new System.EventHandler(this.curmulativeCurveToolStripMenuItem_Click);
            // 
            // _msPlot
            // 
            this._msPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this._msPlot.Location = new System.Drawing.Point(0, 25);
            this._msPlot.Name = "_msPlot";
            this._msPlot.Size = new System.Drawing.Size(887, 433);
            this._msPlot.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 458);
            this.Controls.Add(this._msPlot);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Form1";
            this.Text = "MSFeature";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem openKronicResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openPercolatorResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMS2FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem countToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton_Statistics;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem outputTargetFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputIdFeaturesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openRawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getUnMatchedMS2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIFToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outputSampledPeptideFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private MSPlot _msPlot;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureMzPlotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sampledMzRegionPlotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openNoMatchedMS2FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureNumberAcrossRTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mS2AcrossRTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mS1OverTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton_project;
        private System.Windows.Forms.ToolStripMenuItem outputProjectStatisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureRTDistributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureRTCrossMZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plotAllFiguresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dPlotOfFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analysisTimesDistributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem distributionInIsolationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargeComparisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseOnRawFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseOnKronikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem distributionAmongFeatureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlyTargetedIonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlyTargetedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openHardklorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intensityIdDistributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compareTwoProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem precursorIonsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peptdieFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputIdentificationComparisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem identificationOverTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargesOnMS2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intensityVsNumbersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureIntensityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ionDistributionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abundanceDistributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iTVsChargeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputPeptideFeatureAbundanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem differentChargesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem differentChargesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem coisolationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem precursorVsIdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ratiosVsIdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureIntensityVsSpectraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem identificationAcrossMzBinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scansToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peptidesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proteinsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureCountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allPrecursorIntensityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featuresAndMS2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mS2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mzToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargeVsIdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickTestKronikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateAccuracyOfSamplingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dynamicRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputPrecursorCountDistributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem featureAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton5;
        private System.Windows.Forms.ToolStripMenuItem curmulativeCurveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureCountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem count234PeptideFeatureOnlyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featurePlotsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureChargeDisOverMzToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureChargeDisOverTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputFeatureInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputFeatureNumbersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outputFeatureDetailsToolStripMenuItem1;
    }
}

