﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileReader;

namespace MSFeature
{
    public class NoiseDetection
    {
        static string _outputdir = @"O:\Lab\Result\MSFeature\";
        public static void DetectNoise(Project project)
        {
            float intMin = float.MaxValue;
            List<float> intensities = new List<float>();
            List<float> rts = new List<float>();
            int removed = 0;
            int rtunit = 600;
            foreach (PepFeature feature in project.FeatureManager.PeptideFeatures())
            {
                float intenisty = feature.SummedIntensity;
                if (intMin > intenisty)
                    intMin = intenisty;
                intensities.Add(intenisty);
                rts.Add((float)(feature.RT[1]-feature.RT[0])*600);
            }
            intensities.Sort();
            rts.Sort();
            float interval = 1000f;
            float intbaseline = calculateBaseLine(intensities, interval);

            Console.WriteLine("int baseline: "+intbaseline+"\n");

            interval = 10f;
            string filepath = _outputdir + @"/rt_1";
            float rtbaseline = calculateBaseLine(rts,interval,filepath);
            Console.WriteLine("RT baseline: "+ rtbaseline/rtunit);
            
            rts = new List<float>();
            foreach (PepFeature feature in project.FeatureManager.PeptideFeatures())
            {
                feature.Noise = false;
                if (feature.SummedIntensity <= intbaseline)
                {
                    feature.Noise = true;
                    removed++;
                    continue;
                }
                double rt = (feature.RT[1] - feature.RT[0]) *rtunit ;
                if (rt <= rtbaseline)
                {
                    feature.Noise = true;
                    removed++;
                }
                rts.Add((float)rt);
                if (rt/rtunit >= 2)
                {
                    feature.Noise = true;
                    removed++;
                }
            }

            rts.Sort();

            Console.WriteLine("RT short: " + rts[0] + "\tRT long" + rts[rts.Count - 1]);
            Console.WriteLine("Before remove feature with long rt: "+rts.Count);

            Console.WriteLine("removed feature: "+removed);

            Console.WriteLine("Detected features: "+project.FeatureManager.Statistics.DetectedFeatures);
            Console.WriteLine("Total removed feature: "+removed);
            Console.WriteLine();

        }

        private static float calculateBaseLine(List<float> intensities, float interval, string filepath)
        {
            float baseline = 0f;
            float intMin = intensities[0];
            List<int> intcount = new List<int>();
            double threshold = intMin + interval;
            int count = 0;
            int maxvalue = 0;
            int maxidx = 0;
            for (int i = 0; i < intensities.Count; i++)
            {
                if (intensities[i] <= threshold)
                    count++;
                else
                {
                    intcount.Add(count);
                    threshold += interval;
                    if (maxvalue < count)
                    {
                        maxvalue = count;
                        maxidx = intcount.Count-1;
                    }
                    count = 0;
                }
            }
            intcount.Add(count);
            if (maxvalue < count)
            {
                maxvalue = count;
                maxidx = intcount.Count-1;
            }
            float helf = maxvalue / 2f;
            int shift = 0;
            //int removecount = 0;
            //for (int i = maxidx-1; i >= 0; i--)
            //{
            //    //removecount += intcount[i];
            //    if (intcount[i] <= helf)
            //    {
            //        shift = maxidx - i;
            //        break;
            //    }
            //}
            //if (shift == 0)
            //{
            //    for (int i = maxidx + 1; i <= intcount.Count; i++)
            //    {
            //        if (intcount[i] <= helf)
            //        {
            //            shift = i-maxidx;
            //            break;
            //        }
            //    }
            //}
            //Console.WriteLine("remove count: "+removecount);
            //baseline = intMin + interval*(maxidx+shift+1);
            baseline = intMin + interval * (maxidx+1);

            StreamWriter sw = new StreamWriter(filepath);
            for (int i = 0; i < intcount.Count; i++)
            {
                sw.WriteLine(intcount[i]);
            }
            sw.Close();
            //Console.WriteLine("max index: " + maxidx);
            //Console.WriteLine("shift: " + shift);
            //Console.WriteLine("baseline: " + baseline);
            
            return baseline;
        }


        private static float calculateBaseLine(List<float> intensities, float interval)
        {
            string filepath = _outputdir + @"\intensity";
            return calculateBaseLine(intensities, interval, filepath);
        }

        private float FoundFWHM(List<int> values)
        {
            int maxvalue = 0;
            int maxidx = 0;
            for (int i = 0; i < values.Count; i++)
            {
                if (values[i] > maxvalue)
                {
                    maxvalue = values[i];
                    maxidx = i;
                }
            }
            float helf = maxvalue/2f;

            int rightidx = 0;
            int leftidx = 0;
            for (int i = maxidx; i >= 0; i --)
            {
                if (values[i] <= helf)
                {
                    leftidx = i;
                    break;
                }
            }
            for (int i = maxidx; i <= values.Count; i++)
            {
                if (values[i] <= helf)
                {
                    rightidx = i;
                    break;
                }
            }
            float fwhm = (rightidx - leftidx);
            return fwhm;
        }


    }
}
