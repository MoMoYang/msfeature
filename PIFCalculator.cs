﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chemicals;
using FileReader;
using PeptideFeature;
//using pwiz.CLI.cv;
using pwiz.CLI.msdata;


namespace MSFeature
{
    public class PIFCalculator
    {

        //this method requires hardklor output file
        public void Compute(FeatureManager featureManager)
        {


            



        }

        public void SetupPrecursorScans(FeatureManager featureManager,string rawfilepath)
        {
            if (featureManager == null)
                return;


            //MSDataFile msd = new MSDataFile(rawfilepath);
            ////MSDataFile msd = featureManager.RawFile;
            //int size = msd.run.spectrumList.size();
            //List<int> msOne = new List<int>();
            //List<IsolatedRegion> sortedScans = featureManager.GetSortedScans();
            //Dictionary<int, int> oneToIndex = new Dictionary<int, int>();

            //for (int i = 0; i < size; i++)
            //{
            //    Spectrum sp = msd.run.spectrumList.spectrum(i);
            //    //var pars = sp.cvParam(CVID.MS_ms_level);
            //    var paras = sp.cvParam(pwiz.CLI.cv.CVID.MS_ms_level);
            //    int level = (int)paras.value;

            //    if (level == 1)
            //    {
            //        int scanNo = int.Parse(Functions.GetScanNumber(sp.id));
            //        msOne.Add(scanNo);
            //        oneToIndex.Add(scanNo, i);
            //    }
            //}
            //msOne.Sort();

            //// find the ms1 scan number for each ms/ms
            //Dictionary<int, List<IsolatedRegion>> oneToMSMS = new Dictionary<int, List<IsolatedRegion>>();

            //int idx = 0;
            //for (int i = 0; i < sortedScans.Count; i++)
            //{
            //    for (int j = idx; j < msOne.Count; j++)
            //    {
            //        if (sortedScans[i].ScanNo < msOne[j])
            //        {
            //            int parentScan = msOne[j - 1];
            //            if (!oneToMSMS.ContainsKey(parentScan))
            //                oneToMSMS.Add(parentScan, new List<IsolatedRegion>());
            //            oneToMSMS[parentScan].Add(sortedScans[i]);
            //            idx = j - 1;
            //            break;
            //        }
            //    }
            //}

            //// start calculating precursor intensity fraction
            //foreach (int msone in oneToMSMS.Keys)
            //{
            //    double width = Parameter.isolation_window / 2d;
            //    int scanIdx = oneToIndex[msone];
            //    using (Spectrum element = msd.run.spectrumList.spectrum(scanIdx, true))
            //    {
            //        //Spectrum element = msd.run.spectrumList.spectrum(msone, true);
            //        IList<double> mzArray = element.getMZArray().data;
            //        IList<double> intensityArray = element.getIntensityArray().data;

            //        // only sort centroid spectra; profile spectra are assumed to already be sorted
            //        //if (element.hasCVParam(CVID.MS_centroid_spectrum))
            //        //{
            //        //    // TODO: use Zip extension method after .NET 4 upgrade
            //        //    var map = new SortedDictionary<double, double>(Enumerable.Range(0, mzArray.Count - 1).ToDictionary(i => mzArray[i], i => intensityArray[i]));
            //        //    mzArray = map.Keys.ToList();
            //        //    intensityArray = map.Values.ToList();
            //        //}


            //        foreach (IsolatedRegion scan in oneToMSMS[msone])
            //        {
            //            double left = scan.TargetPrecursorMz - width;
            //            double right = scan.TargetPrecursorMz + width;
            //            double total = 0d;

            //            // get mz positions of isotopeaks
            //            double[] mzpos = mzPosition(scan.TargetIon.MonoIsotopicMass, scan.TargetIon.Charge, 5);

            //            // check if the mz positions outside of isolation window


            //            double tolerance = 0d;
            //            if (Parameter.Tolerance_Unit == Parameter.Units.Da)
            //                tolerance = Parameter.Pep_Tolerance;
            //            else
            //                tolerance = Functions.ppmToDa(10, scan.TargetIon.Mz);
            //            double[] isotopes = new double[mzpos.Length];

            //            // calculate the intensity in isolation window

            //            List<double> mzs = new List<double>();
            //            double min = double.MaxValue;
            //            int currentidx = 0;
            //            for (int index = 0; index < mzArray.Count; index++)
            //            {
            //                if (intensityArray[index] < 1) // skip peak where intensity equal 0
            //                    continue;
            //                if (mzArray[index] < left)
            //                    continue;
            //                else if (mzArray[index] > right)
            //                    break;
            //                total += intensityArray[index];

            //                //for checking
            //                mzs.Add(mzArray[index]);

            //                for (int j = currentidx; j < mzpos.Length; j++)
            //                {
            //                    double dif = Math.Abs(mzpos[j] - mzArray[index]);
            //                    if (dif < tolerance && dif < min)
            //                    {
            //                        isotopes[j] = intensityArray[index];
            //                    }
            //                    else if (mzpos[j] > mzArray[index])
            //                    {
            //                        currentidx = j;
            //                        break;
            //                    }
            //                }
            //            }

            //            foreach (double intensity in isotopes)
            //                scan.PrecursorIsotopeInt += intensity;
            //            scan.TotalIntInIsolationWindow = total;
            //        }
            //    }
            //}
        }

        private double[] mzPosition(double mono, int charge, int noPeaks)
        {
            double[] positions = new double[noPeaks];

            double monoPeak = (mono + charge * Molecules.Proton) / charge;
            double dif = Molecules.Proton / charge;
            for (int i = 0; i < noPeaks; i++)
            {
                positions[i] = monoPeak + dif * i;
            }
            return positions;
        }
        //private static double[] mzPosi
    }
}
