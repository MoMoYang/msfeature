﻿namespace MSFeature
{
    partial class UC_Projectlist
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_project = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // listBox_project
            // 
            this.listBox_project.FormattingEnabled = true;
            this.listBox_project.Location = new System.Drawing.Point(21, 34);
            this.listBox_project.Name = "listBox_project";
            this.listBox_project.Size = new System.Drawing.Size(297, 199);
            this.listBox_project.TabIndex = 0;
            this.listBox_project.SelectedIndexChanged += new System.EventHandler(this.listBox_project_SelectedIndexChanged);
            // 
            // UC_Projectlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listBox_project);
            this.Name = "UC_Projectlist";
            this.Size = new System.Drawing.Size(346, 278);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_project;
    }
}
