﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSFeature
{

    /// <summary>
    /// </summary>
    /// <param name="sender">IEnumerable<Project></param>
    public delegate void UpdateProjectHandler(object sender);



    public class ProjectManager
    {
        public Project CurrentProject = null;

        private Dictionary<string,Project> _projectdictionary = new Dictionary<string, Project>();
        
        public event UpdateProjectHandler ProjectListChanged;

        // invoke event when project list is updated
        protected void OnChange()
        {
            if (ProjectListChanged != null)
                ProjectListChanged(_projectdictionary.Values);
        }

        public void AddProject(string id, Project project)
        {
            if (_projectdictionary.ContainsKey(id))
                _projectdictionary[id] = project;
            else
                _projectdictionary.Add(id,project);
            CurrentProject = project;
            OnChange();
        }

        public Project GetProject(string id)
        {
            if (_projectdictionary.ContainsKey(id))
                return _projectdictionary[id];
            else
                return null;
        }

        public bool ContainProject(string id)
        {
            return _projectdictionary.ContainsKey(id);
        }

        public IEnumerable<Project> AllProjects
        {
            get { return _projectdictionary.Values; }
        }

        public void ChangeCurrentProject(string projectName)
        {
            CurrentProject = GetProject(projectName);
        }
    }
}
