﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MSFeature
{
    public class ColorCollection
    {

        private static List<Color> _colors = new List<Color>()
        {
            Color.DodgerBlue,
            Color.OrangeRed,
            Color.LimeGreen,
            
            Color.BlueViolet,
            Color.Gold,
            Color.DeepPink,

            Color.Goldenrod,
            
            
            
            
            Color.DarkTurquoise,
            Color.DarkGreen,
            Color.Red
        };

        public static List<Color> Colorlist
        {
            get { return _colors; }
        }
    }
}
