﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PeptideFeature;
using FileReader;

namespace MSFeature
{
    public class Project
    {
        public FeatureManager FeatureManager = null;
        public RawDataInfo RawInfo = null;
        private MS2TitleInfo _noMatchedRegion = null;
        
        
        public readonly string ID = "";

        
        public Project(FeatureManager featureManager)
        {
            FeatureManager = featureManager;
            ID = featureManager.Title;
        }

        public void UpdateRawDataInfo(RawDataInfo rawiInfo)
        {
            RawInfo = rawiInfo;
        }
        
        public void UpdateFailAssignedRegion(MS2TitleInfo isolatedRegions)
        {
            if (isolatedRegions != null)
            {
                _noMatchedRegion = isolatedRegions;
            }
        }

        public int NumboerOfNoMatchedMStwo
        {
            get
            {
                if (_noMatchedRegion != null)
                    return _noMatchedRegion.ScanList.Count;
                else
                    return -1;
            }
        }
        

        public IEnumerable<IsolatedRegion> NomatchedRegions()
        {
            if (_noMatchedRegion != null)
                return _noMatchedRegion.ScanList;
            else
                return new List<IsolatedRegion>();
        }

      

    }
}
